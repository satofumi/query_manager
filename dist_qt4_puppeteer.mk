include package_version.mk

DIST_DIR = $(PACKAGE_DIR)/puppeteer
QT_BIN_DIR = `which qmake | sed -e 's/qmake.exe//g'`
QT4_FILES = \
	$(QT_BIN_DIR)/QtCore4.dll \
	$(QT_BIN_DIR)/QtGui4.dll \
	$(QT_BIN_DIR)/QtNetwork4.dll \
	$(QT_BIN_DIR)/libgcc_s_dw2-1.dll \
	$(QT_BIN_DIR)/libstdc++-6.dll \
	$(QT_BIN_DIR)/libwinpthread-1.dll \

all :
	mkdir -p $(DIST_DIR) $(QT5_PLATFORMS_DIR)
	cp puppeteer/release/puppeteer.exe puppeteer/puppeteer_ja.qm $(DIST_DIR)
	cp $(QT4_FILES) $(DIST_DIR)
