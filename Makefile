include package_version.mk

QMAKE_COMMAND = qmake
QT_VERSION = `which qmake | cut -b9`


all :
	cd puppeteer/ && $(QMAKE_COMMAND) && $(MAKE)

clean :
	cd puppeteer/ && $(MAKE) clean

clean-dist :
	$(RM) -rf $(PACKAGE_DIR)/puppeteer
	$(RM) -rf $(PACKAGE_DIR)/web

dist : clean-dist all
	mkdir -p $(PACKAGE_DIR)
	if [ "$(QT_VERSION)" -eq "5" ]; then $(MAKE) -f dist_qt5_puppeteer.mk; \
	else $(MAKE) -f dist_qt4_puppeteer.mk; fi
	$(MAKE) -f dist_mascots.mk
	$(MAKE) -f dist_web.mk
	$(MAKE) -f dist_package.mk

upload :
	rsync -avz -e ssh --delete html/*.html html/*.txt html/*.js html/*.css html/*.png hyakuren-soft@hyakuren-soft.sakura.ne.jp:/home/hyakuren-soft/www/query_manager/
