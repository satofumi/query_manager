/*!
  \file
  \brief dat ファイルの作成
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <QFileInfo>
#include <QTextCodec>
#include "Zip.h"
#include "mascot_setting.h"

using namespace std;


namespace
{
    void print_usage(const string& program_name)
    {
        cout << "usage:" << endl
             << program_name << " dest.dat setting.yaml" << endl;
    }

    void add_file(Zip& zip, const string& file_name)
    {
        QFileInfo file_info(file_name.c_str());
        if (!file_info.exists()) {
            const string message = "file not exist: " + file_name;
            cout << message << endl;
            return;
        }

#ifdef Q_OS_WIN32
        const char* converted = QString(file_name.c_str()).toLocal8Bit();
#else
        const char* converted = file_name.c_str();
#endif
        zip.add_file(converted, file_name);
    }
}


int main(int argc, char *argv[])
{
    if (argc <= 2) {
        print_usage(argv[0]);
        return 0;
    }

    Zip zip;
    if (!zip.compress(argv[1])) {
        return 1;
    }

    ifstream setting_file;
    setting_file.open(argv[2]);

    stringstream stream;
    stream << setting_file.rdbuf();
    string yaml_text = stream.str();

    mascot_setting::mascot_t mascot;
    mascot_setting::load(mascot, yaml_text);

    zip.add_file("setting.yaml", argv[2]);
    zip.add_file("logo.png", "logo.png");
    for (mascot_setting::cells_t::const_iterator it = mascot.cells.begin();
         it != mascot.cells.end(); ++it) {
        add_file(zip, it->second);
    }
    for (mascot_setting::sounds_t::const_iterator it = mascot.sounds.begin();
         it != mascot.sounds.end(); ++it) {
        add_file(zip, it->second);
    }

    return 0;
}
