/*!
  \file
  \brief dat ファイルの作成
*/

#include <iostream>
#include "Zip.h"

using namespace std;


namespace
{
    void print_usage(const string& program_name)
    {
        cout << "usage:" << endl
             << program_name << " dest.dat data1.yaml data2.png data3.png ..."
             << endl;
    }
}


int main(int argc, char *argv[])
{
    if (argc <= 2) {
        print_usage(argv[0]);
        return 0;
    }

    Zip zip;
    if (!zip.compress(argv[1])) {
        return 1;
    }

    for (int i = 2; i < argc; ++i) {
        const string file_name = argv[i];
        zip.add_file(file_name, file_name);
    }
    return 0;
}
