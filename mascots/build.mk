DATA_FILES = \
	setting.yaml \
	logo.png \
	query_normal.png query_blink.png query_cute.png \
	query_happy.png query_armcrossed.png \

all : $(TARGET)

clean :
	$(RM) $(TARGET)

%.zip : $(DATA_FILES)
	../dat_build $@ $(DATA_FILES)
