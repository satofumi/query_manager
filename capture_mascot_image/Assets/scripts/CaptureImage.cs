﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class CaptureImage : MonoBehaviour
{
    public Camera Camera;
	public Text LogoText;

    public GameObject StandardPrefab;
    public GameObject AichiPrefab;
    public GameObject AkitaPrefab;
    public GameObject AomoriPrefab;
    public GameObject BlackPrefab;
    public GameObject ChibaPrefab;
    public GameObject EhimePrefab;
    public GameObject FukuiPrefab;
    public GameObject FukuokaPrefab;
    public GameObject FukushimaPrefab;
    public GameObject GifuPrefab;
    public GameObject GunmaPrefab;
    public GameObject HiroshimaPrefab;
    public GameObject HokkaidoPrefab;
    public GameObject HyogoPrefab;
    public GameObject IshikawaPrefab;
    public GameObject IbarakiPrefab;
    public GameObject IwatePrefab;
    public GameObject KagawaPrefab;
    public GameObject KagoshimaPrefab;
    public GameObject KanagawaPrefab;
    public GameObject KochiPrefab;
    public GameObject KumamotoPrefab;
    public GameObject KyotoPrefab;
    public GameObject MiePrefab;
    public GameObject MikanButoriPrefab;
    public GameObject MiyagiPrefab;
    public GameObject MiyazakiPrefab;
    public GameObject NaganoPrefab;
    public GameObject NagasakiPrefab;
    public GameObject NaraPrefab;
    public GameObject NewFukuokaPrefab;
    public GameObject NewHokkaidoPrefab;
    public GameObject NewOsakaPrefab;
    public GameObject NiigataPrefab;
    public GameObject OkayamaPrefab;
    public GameObject OkinawaPrefab;
    public GameObject OoitaPrefab;
    public GameObject OsakaPrefab;
    public GameObject SagaPrefab;
    public GameObject SaitamaPrefab;
    public GameObject ShigaPrefab;
    public GameObject ShimanePrefab;
    public GameObject ShizuokaPrefab;
    public GameObject SpaceCommandPrefab;
    public GameObject SpainMadridPrefab;
    public GameObject TochigiPrefab;
    public GameObject TokushimaPrefab;
    public GameObject TokyoPrefab;
    public GameObject TottoriPrefab;
    public GameObject ToyamaPrefab;
    public GameObject WakayamaPrefab;
    public GameObject YamagataPrefab;
    public GameObject YamaguchiPrefab;
    public GameObject YamanashiPrefab;

    private GameObject queryChan;
    private QuerySDMecanimController unitAnimation;
    private QuerySDEmotionalController emotion;
    private float shortDelay = 1.0f;

	private const int ScreenWidth = 256;
	private const int ScreenHeight = 256;


    private GameObject InitializeModel(GameObject modelPrefab)
    {
        queryChan = (GameObject)Instantiate(modelPrefab, new Vector3(0, -4, 0), new Quaternion(0, 180, 0, 0));
        queryChan.transform.localScale = new Vector3(9, 9, 9);

        unitAnimation = queryChan.GetComponent<QuerySDMecanimController>();
        emotion = queryChan.GetComponent<QuerySDEmotionalController>();
	    
        return queryChan;
    }

    private void Capture(string file_path, int width = ScreenWidth, int height = ScreenHeight)
    {
        var texture = new Texture2D(width, height, TextureFormat.ARGB32, false);
        texture.ReadPixels(new Rect(0, ScreenHeight - height, width, height), 0, 0);
        texture.Apply();

        var bytes = texture.EncodeToPNG();
        Destroy(texture);
        File.WriteAllBytes(file_path, bytes);
    }

    private IEnumerator ChangePose(QuerySDMecanimController.QueryChanSDAnimationType pose,
                                   QuerySDEmotionalController.QueryChanSDEmotionalType face)
    {
        unitAnimation.ChangeAnimation(pose);
        emotion.ChangeEmotion(face);
        yield return new WaitForSeconds(shortDelay);
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator ChangeLogoPose(GameObject queryChan, string logoTitle)
	{
        LogoText.text = "<b>" + logoTitle + "</b>";

        yield return ChangePose(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_STAND,
                                QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_DEFAULT);
        Transform transform = queryChan.GetComponent<Transform>();
        transform.position = new Vector3(3.5f, -2, 0);
        transform.Rotate(new Vector3(0, 1, 0), 40);

        Camera.backgroundColor = new Color(0.1f, 0.2f, 0.6f, 1.0f);

        yield return new WaitForSeconds(shortDelay);
        yield return new WaitForEndOfFrame();

		Camera.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
	}
	
    private void CaptureLogo(string file_path)
	{
        Capture(file_path, Screen.width, Screen.height / 2);
        LogoText.text = "";
	}

    class Package
    {
        public string folderName;
        public GameObject prefab;
        public string packageName;

        public Package(string folderName, GameObject prefab, string packageName)
        {
            this.folderName = folderName;
            this.prefab = prefab;
            this.packageName = packageName;
        }
    }

    IEnumerator Start()
    {
        LogoText.text = "";

        List<Package> packages = new List<Package>()
        {
            new Package("sd_query/", StandardPrefab, "クエリちゃん"),
            new Package("sd_query_aichi/", AichiPrefab, "あいち"),
            new Package("sd_query_akita/", AkitaPrefab, "あきた"),
            new Package("sd_query_aomori/", AomoriPrefab, "あおもり"),
            new Package("sd_query_black/", BlackPrefab, "ブラック"),
            new Package("sd_query_chiba/", ChibaPrefab, "ちば"),
            new Package("sd_query_ehime/", EhimePrefab, "えひめ"),
            new Package("sd_query_fukui/", FukuiPrefab, "ふくい"),
            new Package("sd_query_fukuoka/", FukuokaPrefab, "ふくおか"),
            new Package("sd_query_fukushima/", FukushimaPrefab, "ふくしま"),
            new Package("sd_query_gifu/", GifuPrefab, "ぎふ"),
            new Package("sd_query_gunma/", GunmaPrefab, "ぐんま"),
            new Package("sd_query_hiroshima/", HiroshimaPrefab, "ひろしま"),
            new Package("sd_query_hokkaido/", HokkaidoPrefab, "ほっかいどう"),
            new Package("sd_query_hyogo/", HyogoPrefab, "ひょうご"),
            new Package("sd_query_ishikawa/", IshikawaPrefab, "いしかわ"),
            new Package("sd_query_ibaraki/", IbarakiPrefab, "いばらき"),
            new Package("sd_query_iwate/", IwatePrefab, "いわて"),
            new Package("sd_query_kagawa/", KagawaPrefab, "かがわ"),
            new Package("sd_query_kagoshima/", KagoshimaPrefab, "かごしま"),
            new Package("sd_query_kanagawa/", KanagawaPrefab, "かながわ"),
            new Package("sd_query_kochi/", KochiPrefab, "こうち"),
            new Package("sd_query_kumamoto/", KumamotoPrefab, "くまもと"),
            new Package("sd_query_kyoto/", KyotoPrefab, "きょうと"),
            new Package("sd_query_mie/", MiePrefab, "みえ"),
            new Package("sd_query_mikan_butori/", MikanButoriPrefab, "みかん太り"),
            new Package("sd_query_miyagi/", MiyagiPrefab, "みやぎ"),
            new Package("sd_query_miyazaki/", MiyazakiPrefab, "みやざき"),
            new Package("sd_query_nagano/", NaganoPrefab, "ながの"),
            new Package("sd_query_nagasaki/", NagasakiPrefab, "ながさき"),
            new Package("sd_query_nara/", NaraPrefab, "なら"),
            new Package("sd_query_new_fukuoka/", NewFukuokaPrefab, "New ふくおか"),
            new Package("sd_query_new_hokkaido/", NewHokkaidoPrefab, "New ほっかいどう"),
            new Package("sd_query_new_osaka/", NewOsakaPrefab, "New おおさか"),
            new Package("sd_query_niigata/", NiigataPrefab, "にいがた"),
            new Package("sd_query_okayama/", OkayamaPrefab, "おかやま"),
            new Package("sd_query_okinawa/", OkinawaPrefab, "おきなわ"),
            new Package("sd_query_ooita/", OoitaPrefab, "おおいた"),
            new Package("sd_query_osaka/", OsakaPrefab, "おおさか"),
            new Package("sd_query_saga/", SagaPrefab, "さが"),
            new Package("sd_query_saitama/", SaitamaPrefab, "さいたま"),
            new Package("sd_query_shiga/", ShigaPrefab, "しが"),
            new Package("sd_query_shimane/", ShimanePrefab, "しまね"),
            new Package("sd_query_shizuoka/", ShizuokaPrefab, "しずおか"),
            new Package("sd_query_space_command/", SpaceCommandPrefab, "うちゅう"),
            new Package("sd_query_spain_madrid/", SpainMadridPrefab, "スペイン"),
            new Package("sd_query_tochigi/", TochigiPrefab, "とちぎ"),
            new Package("sd_query_tokushima/", TokushimaPrefab, "とくしま"),
            new Package("sd_query_tokyo/", TokyoPrefab, "とうきょう"),
            new Package("sd_query_tottori/", TottoriPrefab, "とっとり"),
            new Package("sd_query_toyama/", ToyamaPrefab, "とやま"),
            new Package("sd_query_wakayama/", WakayamaPrefab, "わかやま"),
            new Package("sd_query_yamagata/", YamagataPrefab, "やまがた"),
            new Package("sd_query_yamaguchi/", YamaguchiPrefab, "やまぐち"),
            new Package("sd_query_yamanashi/", YamanashiPrefab, "やまなし"),
        };

        const string base_path = "../mascots/";
        foreach (var package in packages)
        {
            yield return CaptureImages(base_path + package.folderName, package.prefab, package.packageName);
        }
    }

    IEnumerator CaptureImages(string output_path, GameObject prefab, string logo_title)
    {
        GameObject queryChan = InitializeModel(prefab);

		// Pose
        yield return ChangePose(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_STAND,
                                QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_DEFAULT);
        Capture(output_path + "query_normal.png");

        yield return ChangePose(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_STAND,
                                QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_BLINK);
        Capture(output_path + "query_blink.png");

        yield return ChangePose(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_POSE_CUTE,
                                QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_DEFAULT);
        Capture(output_path + "query_cute.png");

        yield return ChangePose(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_POSE_HELLO,
                                QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_SMILE);
        Capture(output_path + "query_happy.png");

        yield return ChangePose(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_POSE_ARMCROSSED,
                                QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_BLINK);
        Capture(output_path + "query_armcrossed.png");

		// Logo
		yield return ChangeLogoPose(queryChan, logo_title);
		CaptureLogo(output_path + "logo.png");

        Destroy(queryChan);
    }
}
