include package_version.mk

all :
	cp README.md $(PACKAGE_DIR)/README.txt
	cp ChangeLog.txt LICENSE.txt thirdparty_licenses.txt mascots/Query-Chan_license_logo.png $(PACKAGE_DIR)
	cp installer/*.vbs $(PACKAGE_DIR)/
	zip -r $(PACKAGE_DIR).zip $(PACKAGE_DIR)/
