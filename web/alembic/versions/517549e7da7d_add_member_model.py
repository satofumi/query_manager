"""add member model

Revision ID: 517549e7da7d
Revises: 2d627d118a0c
Create Date: 2017-07-05 13:25:39.750856

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '517549e7da7d'
down_revision = '2d627d118a0c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('member',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.Text(), nullable=False),
    sa.Column('password', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column(u'setting', sa.Column('authentication', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column(u'setting', 'authentication')
    op.drop_table('member')
    # ### end Alembic commands ###
