from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    Boolean,
    DateTime,
    ForeignKey,
    )

from datetime import datetime

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    )

from zope.sqlalchemy import register

DBSession = scoped_session(sessionmaker(autoflush=True))
register(DBSession)
Base = declarative_base()


class State(Base):
    __tablename__ = 'state'
    id = Column(Integer, primary_key=True)
    label = Column(Text, nullable=False)

class Priority(Base):
    __tablename__ = 'priority'
    id = Column(Integer, primary_key=True)
    label = Column(Text, nullable=False)


class Project(Base):
    __tablename__ = 'project'
    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    active = Column(Boolean, default=True)
    order = Column(Integer, default=0)
    category_id = Column(Integer, nullable=True)

class ProjectCategory(Base):
    __tablename__ = 'project_category'
    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    active = Column(Boolean, default=True)

class Ticket(Base):
    __tablename__ = 'ticket'
    id = Column(Integer, primary_key=True)
    title = Column(Text, nullable=False)
    state_id = Column(Integer, ForeignKey('state.id'))
    priority_id = Column(Integer, ForeignKey('priority.id'))
    project = relationship('Project')
    project_id = Column(Integer, ForeignKey('project.id'))
    star_checked = Column(Boolean, nullable=False, default=False)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow)

class Comment(Base):
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True)
    comment = Column(Text, nullable=False)
    ticket_id = Column(Integer, ForeignKey('ticket.id'))
    created_at = Column(DateTime, default=datetime.utcnow)

class Setting(Base):
    __tablename__ = 'setting'
    id = Column(Integer, primary_key=True)
    locale = Column(Text, nullable=False, default='en')
    authentication = Column(Boolean, default=False)

class Workload(Base):
    __tablename__ = 'workload'
    date = Column(DateTime, primary_key=True)
    project_id = Column(Integer, ForeignKey('project.id'), primary_key=True)
    total = Column(Integer, default=0)
    completed = Column(Integer, default=0)

class Dailywork(Base):
    __tablename__ = 'dailywork'
    date = Column(DateTime, primary_key=True)
    opened = Column(Integer, default=0)
    completed = Column(Integer, default=0)

class Member(Base):
    __tablename__ = 'member'
    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    password = Column(Text, nullable=False)
