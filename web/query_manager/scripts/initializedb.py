# -*- coding: utf-8 -*-
import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from datetime import date

from pyramid.scripts.common import parse_vars

from ..models import (
    DBSession,
    State,
    Priority,
    Project,
    Ticket,
    Comment,
    Workload,
    Setting,
    Base,
    )

import locale


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def _is_db_initialized():
    projects = DBSession.query(Project).all()
    return True if len(projects) > 0 else False


def _register_states():
    for label in ['Open', 'Resolve', 'Reject']:
        DBSession.add(State(label=label))


def _register_priorities():
    for label in ['Critical', 'Major', 'Minor', 'Trivial']:
        DBSession.add(Priority(label=label))


def _register_first_project():
        DBSession.add(Project(name='Inbox'))


def _register_default_setting():
    lang = locale.getdefaultlocale()[0].split('_')[0]
    DBSession.add(Setting(locale=lang))
    return lang


def _register_ticket(ticket, add_days=0):
    from query_manager.views import (_update_workload, _days_date)
    DBSession.add(ticket)
    _update_workload(ticket, +1, _days_date(add_days))


def _register_first_tickets(lang):
    if lang == 'ja':
        DBSession.add(Ticket(title=u'このチケットをクリックしてね',
                             state_id=1,
                             priority_id=2,
                             project_id=1))
        DBSession.add(Comment(comment=u'では、チケットの状態を操作してみましょう。\nこの文章の下の "Resolve" を選択して「更新」ボタンを押し、このチケットを終了させましょう。\n終了させたチケットは数分間は下の方に表示されます。', ticket_id=1))

        DBSession.add(Ticket(title=u'プロジェクトを選択してみよう',
                             state_id=1,
                             priority_id=1,
                             project_id=1))
        DBSession.add(Comment(comment=u'プロジェクト名(Inbox)をクリックするとプロジェクト毎の表示になり、Burn up chart が表示されます。\nBurn up chart とは、プロジェクト毎の全チケット合計と終了したチケット合計のグラフで、プロジェクト進捗の目安になります。', ticket_id=2))

        DBSession.add(Ticket(title=u'チケットを破棄してみよう',
                             state_id=1,
                             priority_id=3,
                             project_id=1))
        DBSession.add(Comment(comment=u'チケットを破棄するには "Reject" を選択してから「更新」ボタンを押します。\n終了させたチケットは数分間は下の方に表示されます。\n（読み終わった他のチケットも破棄してしまいましょう）', ticket_id=3))

        DBSession.add(Ticket(title=u'プロジェクトを作ってみよう',
                             state_id=1,
                             priority_id=3,
                             project_id=1))
        DBSession.add(Comment(comment=u'自分のプロジェクトを作って活用していきましょう。\nプロジェクトは左側の「新規プロジェクトの作成」から作成できます。', ticket_id=4))

        DBSession.add(Workload(date=date.today(), project_id=1, total=4, completed=0))


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
    with transaction.manager:
        if not _is_db_initialized():
            _register_states()
            _register_priorities()
            _register_first_project()
            lang = _register_default_setting()
            _register_first_tickets(lang)
