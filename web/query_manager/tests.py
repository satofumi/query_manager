import unittest
import transaction
from datetime import (
    datetime,
    date,
    timedelta,
    )

from pyramid import testing

class _Priority:
    Critical = 1
    Major = 2
    Minor = 3
    Trivial = 4

class _State:
    Open = 1
    Finish = 2
    Reject = 3


def _initialize_test_db():
        from sqlalchemy import create_engine
        from .models import (
            DBSession,
            Workload,
            Ticket,
            Project,
            Base,
            )
        engine = create_engine('sqlite://')
        Base.metadata.create_all(engine)
        DBSession.configure(bind=engine)
        with transaction.manager:
            DBSession.add(Project(name='Inbox'))
            DBSession.add(Project(name='Project1'))
        return DBSession

def _days_date(add_days = 0):
    return datetime.combine(date.today() + timedelta(days=add_days), datetime.min.time())


class TestWorkloadUpdate(unittest.TestCase):
    def setUp(self):
        self.session = _initialize_test_db()
        self.config = testing.setUp()

    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def _call_create_ticket(self, request):
        from query_manager.views import create_ticket
        return create_ticket(request)

    def _call_update_ticket(self, request):
        from query_manager.views import update_ticket
        return update_ticket(request)

    def _call_update_workload(self, ticket, count, today):
        from query_manager.views import _update_workload
        return _update_workload(ticket, count, today)

    def _create_ticket(self, priority_id, state_id, project_id=1):
        return {
            'title': 'title',
            'state': state_id,
            'priority': priority_id,
            'project_id': project_id,
            }

    def _update_ticket(self, ticket_id, priority_id, state_id, project_id=1):
        return {
            'title': 'Trivial ticket',
            'ticket_id': ticket_id,
            'state': state_id,
            'priority': priority_id,
            'project_id': project_id,
            }


    def test_count_workload(self):
        from query_manager.models import Workload
        request = testing.DummyRequest()
        request.referer = ''

        # add critical ticket
        request.POST = self._create_ticket(_Priority.Critical, _State.Open)
        self._call_create_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 1)
        self.assertEqual(workload.completed, 0)

        # add major ticket(1)
        request.POST = self._create_ticket(_Priority.Major, _State.Open)
        self._call_create_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 2)
        self.assertEqual(workload.completed, 0)

        # add minor ticket(2)
        request.POST = self._create_ticket(_Priority.Minor, _State.Open)
        self._call_create_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 0)

        # add trivial ticket(3)
        request.POST = self._create_ticket(_Priority.Trivial, _State.Open)
        self._call_create_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 0)


        # finish critical ticket
        request.POST = self._update_ticket(1, _Priority.Critical, _State.Finish)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 1)

        # finish major ticket
        request.POST = self._update_ticket(2, _Priority.Major, _State.Finish)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 2)

        # finish minor ticket
        request.POST = self._update_ticket(3, _Priority.Minor, _State.Finish)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 3)

        # finish trivial ticket
        request.POST = self._update_ticket(4, _Priority.Trivial, _State.Finish)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 3)


        # add tickets(5,6,7,8)
        request.POST = self._create_ticket(_Priority.Critical, _State.Open)
        self._call_create_ticket(request)
        request.POST = self._create_ticket(_Priority.Major, _State.Open)
        self._call_create_ticket(request)
        request.POST = self._create_ticket(_Priority.Minor, _State.Open)
        self._call_create_ticket(request)
        request.POST = self._create_ticket(_Priority.Trivial, _State.Open)
        self._call_create_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3 + 3)
        self.assertEqual(workload.completed, 3)

        # reject critical ticket
        request.POST = self._update_ticket(5, _Priority.Critical, _State.Reject)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 5)
        self.assertEqual(workload.completed, 3)

        # reject major ticket
        request.POST = self._update_ticket(6, _Priority.Major, _State.Reject)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 4)
        self.assertEqual(workload.completed, 3)

        # reject minor ticket
        request.POST = self._update_ticket(7, _Priority.Minor, _State.Reject)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 3)

        # reject trivial ticket
        request.POST = self._update_ticket(8, _Priority.Trivial, _State.Reject)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 3)


        # re-finish ticket
        request.POST = self._update_ticket(1, _Priority.Critical, _State.Finish)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 3)


        # move ticket(9)
        request.POST = self._create_ticket(_Priority.Critical, _State.Open)
        self._call_create_ticket(request)
        self.assertEqual(workload.total, 4)
        self.assertEqual(workload.completed, 3)

        request.POST = self._update_ticket(9, _Priority.Critical, _State.Open, 2)
        self._call_update_ticket(request)
        workload = self.session.query(Workload).first()
        self.assertEqual(workload.total, 3)
        self.assertEqual(workload.completed, 3)


    def test_date_handling(self):
        from query_manager.models import (Workload, Ticket)
        request = testing.DummyRequest()
        request.referer = ''

        # add 1/1 ticket(1)
        ticket = Ticket(title='title',
                        state_id=_State.Open, priority_id=_Priority.Major,
                        project_id=1)
        yesterday = _days_date(-1)
        self._call_update_workload(ticket, 1, yesterday)
        workload = self.session.query(Workload).one()
        self.assertEqual(workload.total, 1)
        self.assertEqual(workload.completed, 0)

        # add 1/2 ticket(2)
        ticket = Ticket(title='title',
                        state_id=_State.Open, priority_id=_Priority.Major,
                        project_id=1)
        today = _days_date()
        self._call_update_workload(ticket, 1, today)
        workload = self.session.query(Workload).all()
        self.assertEqual(len(workload), 2)
        self.assertEqual(workload[0].total, 1)
        self.assertEqual(workload[0].completed, 0)
        self.assertEqual(workload[1].total, 1)
        self.assertEqual(workload[1].completed, 0)


class TestBurnUpData(unittest.TestCase):
    def setUp(self):
        self.session = _initialize_test_db()
        self.config = testing.setUp()

    def tearDown(self):
        self.session.remove()
        testing.tearDown()

    def _call_burn_up_data(self, project_id=1):
        from query_manager.views import _burn_up_data
        return _burn_up_data(project_id)

    def _add_workload(self, day, total, completed):
        from query_manager.models import Workload
        self.session.add(Workload(date=day, project_id=1,
                                  total=total, completed=completed))

    def _date_str(self, day):
        return str(day).split(' ')[0]


    def test_data_range(self):
        pre_31 = _days_date(-31)
        pre_30 = _days_date(-30)
        pre_29 = _days_date(-29)
        pre_20 = _days_date(-20)
        pre_10 = _days_date(-10)
        pre_1 = _days_date(-1)
        today = _days_date()
        next_1 = _days_date(+1)

        self._add_workload(pre_31, 1, 0)
        self._add_workload(pre_30, 2, 1)
        self._add_workload(pre_29, 3, 2)
        self._add_workload(pre_20, 4, 3)
        self._add_workload(pre_10, 5, 4)
        self._add_workload(pre_1, 6, 5)
        self._add_workload(today, 7, 6)

        data = self._call_burn_up_data()

        self.assertEqual(len(data), 30 + 1 + 2)
        self.assertEqual(data[0]['date'], self._date_str(pre_30))
        self.assertEqual(data[0]['total'], 2)
        self.assertEqual(data[0]['completed'], 1)

        self.assertEqual(data[1]['date'], self._date_str(pre_29))
        self.assertEqual(data[1]['total'], 3)
        self.assertEqual(data[1]['completed'], 2)

        self.assertEqual(data[30]['date'], self._date_str(today))
        self.assertEqual(data[30]['total'], 7)
        self.assertEqual(data[30]['completed'], 6)

        self.assertEqual(data[31]['date'], self._date_str(next_1))
        self.assertEqual(data[31]['total'], 0)
        self.assertEqual(data[31]['completed'], 0)


    def test_first_data(self):
        pre_1 = _days_date(-1)
        self._add_workload(pre_1, 2, 1)

        data = self._call_burn_up_data()

        self.assertEqual(len(data), 30 + 1 + 2)
        self.assertEqual(data[0]['total'], 0)
        self.assertEqual(data[0]['completed'], 0)

        self.assertEqual(data[28]['total'], 0)
        self.assertEqual(data[28]['completed'], 0)

        self.assertEqual(data[29]['date'], self._date_str(pre_1))
        self.assertEqual(data[29]['total'], 2)
        self.assertEqual(data[29]['completed'], 1)

        self.assertEqual(data[30]['total'], 2)
        self.assertEqual(data[30]['completed'], 1)

        self.assertEqual(data[31]['total'], 0)
        self.assertEqual(data[31]['completed'], 0)
