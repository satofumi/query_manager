class State:
    Open = 1
    Finish = 2
    Reject = 3

    Labels = [None, 'Opened', 'Resolved', 'Rejected']
