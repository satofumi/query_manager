var form_utils = {}


form_utils.add_submit_shortcut = function () {
    shortcut.add('Ctrl+Enter', function () {
	$('.submit').click();
    });
}

form_utils.remove_submit_shortcut = function () {
    shortcut.remove('Ctrl+Enter');
}

form_utils.add_close_shortcut = function (address) {
    shortcut.add('ESC', function () {
	window.location.href = address;
    });
}

form_utils.add_radio_marker = function () {
    $('.radio-item:checked').parent('span').addClass('radio-marker');
    $('.radio-item').change(function () {
	if ($(this).is(':checked')){
	    $(this).parent('span').addClass('radio-marker');
	    $('.radio-item:not(:checked)').parent('span').removeClass('radio-marker');
	}
    });
}

form_utils.add_enabled_changer = function () {
    $('.title-input').bind('keydown keyup keypress change', function () {
	$('.submit').removeAttr('disabled');
    });
}

form_utils.format = function (str, col) {
    col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);

    return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
        if (m == '{{') {
	    return '{';
	}
        if (m == '}}') {
	    return '}';
	}
        return col[n];
    });
};

form_utils.disable_double_submit = function (form_tag) {
    $(form_tag).on('submit', function () {
	$(this).find('button[type="submit"]').attr('disabled', 'disabled');
    });
}
