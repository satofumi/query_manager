var burn_up_chart = {};

burn_up_chart._plot = null;


burn_up_chart._resize_width = function() {
    $('.burn-up-chart').attr('style', 'width:' + $('.tickets').width() + 'px;height:180px');
}

burn_up_chart.draw = function(project_id, data, options) {
    if (burn_up_chart._plot) {
	burn_up_chart._plot.destroy();
    }

    var total_data = [];
    var completed_data = [];
    var xaxis_data = [];

    var today = options['today'];
    var total_max = 0;
    var completed_min_default = 10000000000;
    var completed_min = completed_min_default;
    var is_future = false;
    var index = 0;
    for (var key in data) {
	var v = data[key];

	var day = v.day;
	if (v.weekday == 6) {
	    day = '<span style="color:red">' + day + '</span>';
	} else if (v.weekday == 5) {
	    day = '<span style="color:blue">' + day + '</span>';
	}

	if (v.date == today) {
	    day = '<div align="center"><strong>' +
		day + '</strong>' +
		'</div><div style="margin-top:-0.5em">Today</div>';
	} else if (v.date > today) {
	    is_future = true;
	}

	xaxis_data.push([key, day]);

	var null_value = (project_id == 'all' && !is_future) ? 0 : null;
	completed_data.push([key, v.completed > 0 ? v.completed : null_value]);
	total_data.push([key, v.total > 0 ? v.total : null_value]);

	total_max = Math.max(Math.max(total_max, v.total), v.completed);
	if ((v.completed >= 0) && (index < (data.length - 2))) {
	    completed_min = Math.min(completed_min, v.completed);
	}
	index += 1;
    }

    var y_min = 0;
    var y_ticks = '';
    if (total_max < 10) {
	y_ticks = ['0', '2', '4', '6', '8', '10'];
    } else if (total_max < 15) {
	y_ticks = ['0', '5', '10', '15'];
    } else if (total_max < 20) {
	y_ticks = ['0', '5', '10', '15', '20'];
    } else {
	if (completed_min == completed_min_default) {
	    completed_min = total_max;
	}
	if (completed_min > 50) {
	    y_min = Math.round((completed_min - 25) / 50) * 50;
	} else if (completed_min > 5) {
	    y_min = Math.round((completed_min - 5) / 10) * 10;
	}
    }

    var target = 'chart_' + project_id;
    burn_up_chart._plot =
	$.jqplot(target, [completed_data, total_data],
		 {
		     series:[{renderer: $.jqplot.BarRenderer,
			      rendererOptions: {
				  barWidth: 10,
			      },
			     },
			     {}],
		     axes: {
			 xaxis: {
			     ticks: xaxis_data,
			 },
			 yaxis: {
			     min: y_min,
			     ticks: y_ticks,
			 }
		     },
		 });

    $(window).resize(function() {
	burn_up_chart._resize_width()
	burn_up_chart._plot.replot();
    });
}


$(function() {
    burn_up_chart._resize_width();
});
