var project_category = {};


project_category.add = function(route) {
    $.getJSON(route, function(data) {
        var target = $('span#detail');
        target.empty();

	form =
	    '<h4>' + data['_Create_new_project_category'] + '</h4>' +
	    '<form method="post" action="/project_category/create" autocomplete="off">' +
	    '<p class="small-title text-muted">' + data['_Project_category_name'] + '</p>' +
	    '<input type="text" class="title-input current-color form-control" name="name">' +
	    '<br>' +
            '<button class="submit btn btn-primary" type="submit" name="add_button" value="add" title="Ctrl+Enter" disabled="disabled">' + data['_Create'] + '</button>' +
	    '</form>';
        target.append(form);

	$('.title-input').focus();
	form_utils.add_submit_shortcut();
	form_utils.add_close_shortcut(data['base_address']);
	form_utils.add_enabled_changer();
	form_utils.add_radio_marker();
    });
}

project_category.edit = function(route) {
    $.getJSON(route, function(data) {
        var target = $('span#detail');
        target.empty();

	form =
	    '<h4>' + data['_Edit_project_name'] + '</h4>' +
	    '<form method="post" action="/project_category/update" autocomplete="off">' +
            '<input type="hidden" name="project_category_id" value="' + data['project_category_id'] + '">' +
	    '<p class="small-title text-muted">' + data['_Project_category_name'] + '</p>' +
	    '<input type="text" class="title-input current-color form-control" name="name" value="' + data['project_category_name'] + '">' +
	    '<br>' +
            '<button class="submit btn btn-primary" type="submit" name="add_button" value="add" title="Ctrl+Enter">' + data['_Update'] + '</button>' +
	    '</form>';
        target.append(form);

	var name = $('.title-input').val();
	$('.title-input').focus().val('').val(name);
	form_utils.add_submit_shortcut();
	form_utils.add_close_shortcut(data['base_address']);
	form_utils.add_enabled_changer();
	form_utils.add_radio_marker();
    });
}

project_category.confirm = function(tag, message) {
    $(tag).click(function() {
        var link_tokens = $(this).attr('href').split('/');
        var project_category_id = link_tokens[link_tokens.length - 1];
        var name = $('span#r' + project_id).text();
        if (!confirm(form_utils.format(message, name))) {
            return false;
        }
    });
}


$(function() {
    $('a.add').click(function () {
        project_category.add($(this).attr('href'));
        return false;
    });

    $('a.edit').click(function () {
        project_category.edit($(this).attr('href'));
        return false;
    });

    shortcut.add('N', function() {
	project_category.add('/category/add');
    }, {
	'disable_in_input':true,
    });
});
