var project = {};


project.add = function(route) {
    $.getJSON(route, function(data) {
        var target = $('span#detail');
        target.empty();

	form =
	    '<h4>' + data['_Create_new_project'] + '</h4>' +
	    '<form method="post" action="/project/create" autocomplete="off">' +
	    '<p class="small-title text-muted">' + data['_Project_name'] + '</p>' +
	    '<input type="text" class="title-input current-color form-control" name="name">' +
	    '<br>' +
            '<button class="submit btn btn-primary" type="submit" name="add_button" value="add" title="Ctrl+Enter" disabled="disabled">' + data['_Create'] + '</button>' +
	    '</form>';
        target.append(form);

	$('.title-input').focus();
	form_utils.add_submit_shortcut();
	form_utils.add_close_shortcut(data['base_address']);
	form_utils.add_enabled_changer();
	form_utils.add_radio_marker();
    });
}

project.edit = function(route) {
    $.getJSON(route, function(data) {
        var target = $('span#detail');
        target.empty();

	form =
	    '<h4>' + data['_Edit_project_name'] + '</h4>' +
	    '<form method="post" action="/project/update" autocomplete="off">' +
            '<input type="hidden" name="project_id" value="' + data['project_id'] + '">' +
	    '<p class="small-title text-muted">' + data['_Project_name'] + '</p>' +
	    '<input type="text" class="title-input current-color form-control" name="name" value="' + data['project_name'] + '">' +
	    '<br>' +
            '<button class="submit btn btn-primary" type="submit" name="add_button" value="add" title="Ctrl+Enter">' + data['_Update'] + '</button>' +
	    '</form>';
        target.append(form);

	var name = $('.title-input').val();
	$('.title-input').focus().val('').val(name);
	form_utils.add_submit_shortcut();
	form_utils.add_close_shortcut(data['base_address']);
	form_utils.add_enabled_changer();
	form_utils.add_radio_marker();
    });
}

project.confirm = function(tag, message) {
    $(tag).click(function() {
        var link_tokens = $(this).attr('href').split('/');
        var project_id = link_tokens[link_tokens.length - 1];
        var name = $('span#r' + project_id).text();
        if (!confirm(form_utils.format(message, name))) {
            return false;
        }
    });
}


$(function() {
    $('#sortable-projects').sortable({placeholder: 'ui-state-highlight'});
    $('#sortable-projects').disableSelection();

    $('a.add').click(function () {
        project.add($(this).attr('href'));
        return false;
    });

    $('a.edit').click(function () {
        project.edit($(this).attr('href'));
        return false;
    });

    $('.save-project-order').click(function() {
	location.href = '';
    })

    $('#sortable-projects').sortable({
	update: function (event, ui) {
            var order = $(this).sortable('toArray').join(',');

	    $('.save-project-order').unbind();
	    $('.save-project-order').click(function() {
		location.href = '/project/save_order/' + order;
	    })
	}
    });

    shortcut.add('N', function() {
	project.add('/project/add');
    }, {
	'disable_in_input':true,
    });
});
