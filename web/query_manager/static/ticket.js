var ticket = {};

ticket.State = {1: 'Open', 2: 'Resolve', 3: 'Reject'};
ticket.Priority = {1: 'Critical', 2: 'Major', 3: 'Minor', 4: 'Trivial'};
ticket.Open_id = 1;
ticket.Minor_id = 3;


ticket._state_form = function(id, enable) {
    var form = '';
    for (var key in ticket.State) {
        var selected = (key == id) ? ' checked' : (enable ? '' : ' disabled');
	var text_disabled = enable ? '' : (key == 1) ? '' : ' text-muted';
	var ed = ''
	if (key == id) {
	    ed = (id == 2) ? 'd' : 'ed';
	}
        form += '<nobr><span class="radio-item"><input class="radio-item" type="radio" name="state" value="' + key + '" id="s' + key + '"' + selected + '> <label class="icon-text' + text_disabled + '" for="s' + key + '"> <img class="align-top" src="/static/state_' + key + '.png"> ' + ticket.State[key] + ed + '</label></span></nobr><br>';
    }
    return form;
}

ticket._priority_form = function(id) {
    var form = '';
    for (var key in ticket.Priority) {
        var selected = (key == id) ? ' checked' : '';
        form += '<nobr><span class="radio-item"><input class="radio-item" type="radio" name="priority" value="' + key + '" id="p' + key + '"' + selected + '> <label class="icon-text" for="p' + key + '"> <img class="align-top" src="/static/priority_' + key + '.png"> ' + ticket.Priority[key] + '</label></nobr></span><br>';
    }
    return form;
}

ticket._projects_form = function(projects, project_name, project_id, ticket_id, data) {
    if (projects.length <= 1) {
	return '<strong>' + project_name + '</strong>';
    } else {
	var links =
	    '<div class="dropdown">' +
	    '<button class="current-project current-color btn btn-default dropdown-toggle" type="button" id="move-ticket" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><strong>' + project_name + '</strong><span class="m-l-1"></span><span class="caret"></span></button>' +
	    '<ul class="dropdown-menu" aria-labelledby="move-ticket">';

	$.each(projects, function(key, project_data) {
	    var id = project_data['id'];
	    var name = project_data['name'];
            if (id != project_id) {
		href = '/ticket/move/' + ticket_id + '/' + id;
		links += '<li><a href="' + href + '">' + form_utils.format(data['_Move_to'], name) + '</a></li>';
            }
	});
	links += '</ul></div>';
	return links;
    }
}

ticket._priority_item = function(ticket_id, priority_id, show_project) {
    return '<li><a class="update" href="/ticket/update/priority/' + ticket_id + '/' + priority_id + '/' + show_project + '"><img class="align-top" src="/static/priority_' + priority_id + '.png"> ' + ticket.Priority[priority_id] + '</a></li>';
}

ticket._apply_ajax = function(tag, func) {
    $(tag).click(function () {
        func($(this).attr('href'));
        return false;
    });
}

ticket._update_submit_link = function(tag, link) {
    $(tag).submit(function () {
        ticket.update(link, this);
	return false;
    });
}

ticket._tickets_table = function(tickets_data, show_project) {
    tickets = ''

    $.each(tickets_data, function(key, t) {
	var ticket_id = t['id'];
	var star_checked = t['star_checked'];
	var title = t['title'];
	var state_id = t['state_id'];
	var priority_id = t['priority_id'];

	var up_priority = '';
	if (priority_id > 1) {
	    up_priority = ticket._priority_item(ticket_id, priority_id - 1, show_project);
	}

	var down_priority = '';
	if (priority_id < 4) {
	    down_priority = ticket._priority_item(ticket_id, priority_id + 1, show_project);
	}

	tickets += '<tr id="t' + ticket_id + '" class="t' + (state_id == 1 ? '' : ' ticket-finished') + '">' +
	    '<td class="text-center">#' + ticket_id + '</td>' +
	    '<td><a class="update item-link" href="/ticket/toggle/star/' + ticket_id + '/' + show_project + '">' +
	    '<i class="glyphicon glyphicon-star" ' + (star_checked ? 'style="color:darkorange">' : 'style="color:gray">') + '</i></a></td>' +
	    '<td class="text-center"><img class="align-top" src="/static/state_' + state_id + '.png" title="' + ticket.State[state_id] + '"></td>' +
	    '<td><a class="edit item-link" href="/ticket/edit/' + ticket_id + '/' + show_project + '">' + title + '</a></td>' +
	    '<td class="text-center rowlink-skip">' +
	    '<div class="dropdown">' +
	    '<button class="current-priority btn btn-default dropdown-toggle" type="button" id="priority_' + ticket_id + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">&nbsp;<img class="align-top" src="/static/priority_' + priority_id + '.png"></button>' +
	    '<ul class="priority-list dropdown-menu" aria-labelledby="priority_' + ticket_id + '">' +
	    up_priority + down_priority + '</ul>' +
	    '</div></td></tr>';
    });
    return tickets;
}


ticket.update_link = function(tag) {
    $(tag).click(function () {
	ticket.update($(this).attr('href'));
	return false;
    });
}

ticket.update = function(route) {
    $.ajax({
        type: 'POST',
        url: route,
        dataType: 'json',
        data: {
            'title': $('[name=title]').val(),
            'state': $('[name=state]:checked').val(),
	    'priority': $('[name=priority]:checked').val(),
	    'project_id': $('[name=project_id]').val(),
	    'show_project': $('[name=show_project]').val(),
	    'ticket_id': $('[name=ticket_id]').val(),
	    'star_checked': $('[name=star_checked]').val(),
	    'comment': $('[name=comment]').val(),
        },
        success: function(data) {
	    // エラー時に何も変更しないときには、data に {} を返すことにした
	    if (Object.keys(data).length == 0) {
		return;
	    }

	    // スター付きチケットのリスト表示の処理
	    var star_target = $('tbody#project_0');
	    var show_project = data['show_project'];
	    star_target.empty();
	    star_tickets =
		ticket._tickets_table(data['star_tickets'], show_project);
	    star_target.append(star_tickets);
	    ticket.hide_tickets('0');

	    // 更新するチケットのリスト表示の処理
	    var project_id = data['project_id']
	    var target = $('tbody#project_' + project_id);
	    var last_tr_line = target.find('tr:last').html();
	    var hide_tickets = target.find('tr.more').size() ? true : false;
	    var toggle_star_control = (route.indexOf('ticket/toggle/star') >= 0) ? true : false;
            target.empty();

	    var tickets = '';

	    tickets = ticket._tickets_table(data['tickets'], show_project);
	    target.append(tickets)
	    target.append('<tr>' + last_tr_line + '</tr>');

	    if (hide_tickets) {
		ticket.hide_tickets(project_id);
	    }

	    ticket._apply_ajax('a.edit', ticket.edit);
	    ticket._apply_ajax('a.add', ticket.add);

	    ticket.update_link('a.update');
	    if (!toggle_star_control) {
		ticket.edit('/ticket/edit/' + data['created_ticket_id'] + '/' + show_project);
	    }

	    burn_up_chart.draw(data['show_project'], data['burn_up_data'], data['burn_up_options']);
        }
    });
}

ticket.add = function(route) {
    $.getJSON(route, function(data) {
	// チケット情報の表示
        var target = $('span#detail');
        target.empty();

        var form =
	    '<table class="full-width"><tbody><tr><td><strong>' + data['project_name'] + '</strong></td><td class="align-top text-right"><a href="' + data['base_address'] + '"><i class="glyphicon glyphicon-remove"></i></a></td></tr></tbody></table>' +
            '<h4 class="title">' + data['_New_ticket'] + '</h4>' +
            '<form id="create" method="post" action="/ticket/create" autocomplete="off">' +
	    '<p class="small-title text-muted">' + data['_Title'] + '</p>' +
	    '<input type="text" class="title-input form-control" name="title">' +
            '<p class="small-title text-muted">' + data['_Comment'] + '</p>' +
	    '<div class="row"><div class="col-md-10">' +
            '<textarea class="form-control" name="comment"></textarea>'+
	    '</div></div>' +
	    '<div class="row"><div class="col-md-3">' +
	    '<p class="small-title text-muted">' + data['_State'] + '</p>' +
            ticket._state_form(ticket.Open_id, false) +
	    '</div><div class="col-md-3">' +
	    '<p class="small-title text-muted">' + data['_Priority'] + '</p>' +
            ticket._priority_form(ticket.Minor_id) +
	    '</div></div>' +
            '<input type="hidden" name="project_id" value="' + data['project_id'] + '">' +
            '<input type="hidden" name="show_project" value="' + data['show_project'] + '">' +
            '<button class="submit btn btn-primary" type="submit" name="add_button" value="add" title="Ctrl+Enter" disabled="disabled">' + data['_Create'] + '</button>' +
            '</form>';
        target.append(form);

	$('.title-input').focus();
	form_utils.disable_double_submit('#create');
	form_utils.add_close_shortcut(data['base_address']);
	form_utils.add_enabled_changer();
	form_utils.add_radio_marker();

	ticket._update_submit_link('#create', '/ticket/create');
    });
}

ticket.edit = function(route) {
    $.getJSON(route, function(data) {
	// 選択されたチケット項目に色をつける
	$('tr.current-ticket').removeClass('current-ticket');
	$('tr#t' + data['ticket_id']).addClass('current-ticket');

	// チケット情報の表示
        var target = $('span#detail');
        target.empty();

        var form =
	    '<table class="full-width"><tbody><tr><td>' +
	    ticket._projects_form(data['projects'], data['project_name'], data['project_id'], data['ticket_id'], data) +
	    '</td><td class="align-top text-right"><a href="' + data['base_address'] + '"><i class="glyphicon glyphicon-remove"></i></a></td></tr></tbody></table>' +
            '<table class="full-width"><tbody><tr><td>' +
	    '<h4 class="title">' + data['_Ticket'] + ' #' + data['ticket_id'] + '</h4></td><td class="text-right"><small class="text-muted">(' + data['_Created'] + ' ' + data['created_at'] + ')</small></td></tr></table>' +
            '<form id="update" method="post" action="/ticket/update" autocomplete="off">' +
	    '<p class="small-title text-muted">' + data['_Title'] + '</p>' +
	    '<input type="text" class="current-color form-control" name="title" value="' + data['title'] + '">';

        $.each(data['comments'], function(key, comment_data) {
            form += '<p class="small-title text-muted">' + comment_data['datetime'] + '</p><hr><div class="m-l-1">' + comment_data['comment'] + '</div>';
        });
	var comments_length = data['comments'].length;
	if (comments_length > 0) {
	    form += '<div class="text-right"><a class="small-title delete-comment" href="/comment/delete/' + data['comments'][comments_length - 1].id + '">' + data['_Remove_this_comment']+ '</a></div>';
	}

        form += '<p class="small-title text-muted">' + data['_Comment'] + '</p>' +
	    '<div class="row"><div class="col-md-10">' +
	    '<textarea class="comment-input form-control" name="comment"></textarea>' +
	    '</div></div>' +
	    '<div class="row"><div class="col-md-3">' +
	    '<p class="small-title text-muted">' + data['_State'] + '</p>' +
            ticket._state_form(data['state_id'], true) +
	    '</div><div class="col-md-3">' +
	    '<p class="small-title text-muted">' + data['_Priority'] + '</p>' +
            ticket._priority_form(data['priority_id']) +
	    '</div></div>' +
            '<input type="hidden" name="ticket_id" value="' + data['ticket_id'] + '">' +
            '<input type="hidden" name="project_id" value="' + data['project_id'] + '">' +
            '<input type="hidden" name="show_project" value="' + data['show_project'] + '">' +
            '<button class="submit btn btn-primary" type="submit" name="edit_button" value="edit" title="Ctrl+Enter">' + data['_Update'] + '</button>' +
            '</form>' +
	    '<br><br><br><br><br><br><br><br>';
        target.append(form);

	$('.delete-comment').click(function () {
            ticket.edit($(this).attr('href'));
            return false;
	});

	$('.comment-input').focus();
	form_utils.disable_double_submit('#update');
	form_utils.add_close_shortcut(data['base_address']);
	form_utils.add_radio_marker();

	ticket._update_submit_link('#update', '/ticket/update');
    });
}

ticket.new_ticket_shortcut = function(project_id, show_project) {
    shortcut.add('N', function() {
	if (project_id != 'all') {
	    ticket.add('/ticket/add/' + project_id + '/' + show_project);
	}
    },{
	'disable_in_input':true,
    });
}

ticket.hide_tickets = function(project_id) {
    var project = (project_id == null) ? '' : '#project_' + project_id + ' ';
    var tr_line = $(project + ' tr.t:nth-child(4)');
    if (tr_line.size()) {
	tr_line.prev('.t').after('<tr class="more"><td></td><td colspan="4"><a>----- &nbsp;More&nbsp; -----</a></td></tr>');
    }

    $('tr.more').nextAll().hide();
    $('tr.more').click(function(){
	$(this).nextAll().show();
	$(this).remove();
    });
}


$(function() {
    ticket._apply_ajax('a.edit', ticket.edit);
    ticket._apply_ajax('a.add', ticket.add);
    $('tbody.rowlink').rowlink();
});
