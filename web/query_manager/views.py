# -*- coding: utf-8 -*-
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
    )
from pyramid.view import (
    view_config,
    view_defaults,
    forbidden_view_config,
    )
from .security import (
    check_password
)

from sqlalchemy import (func, or_)

from sqlalchemy.exc import DBAPIError
from datetime import (
    datetime,
    date,
    timedelta,
    )

import re

from .models import (
    DBSession,
    Project,
    ProjectCategory,
    Ticket,
    Comment,
    Setting,
    Workload,
    Dailywork,
    )

import pytz
from tzlocal import get_localzone
from pyramid.i18n import TranslationStringFactory
from .state import State
from .priority import Priority
from .puppeteer import Puppeteer

_ = TranslationStringFactory('query_manager')

_Invalid_ticket_id = 0

_locales = {'en': 'English', 'ja': u'日本語'}
_locale_keys = ['ja', 'en']

_local_tz = get_localzone()
_locale = None
_puppeteer_object = None
_All = 'all'

_link = re.compile(r'(?:(https?://)|(www\.))(\S+\b/?)([!"#$%&\'()*+,\-./:;<=>?@[\\\]^_`{|}~]*)(\s|$)', re.I)

def _puppeteer():
    global _puppeteer_object
    if not _puppeteer_object:
        _puppeteer_object = Puppeteer()
    return _puppeteer_object

def _locale_name():
    global _locale
    if not _locale:
        setting = DBSession.query(Setting).one_or_none()
        _locale = setting.locale if setting else 'en'
    return _locale

def _convert_links(text):
    def replace(match):
        groups = match.groups()
        protocol = groups[0] or ''
        www_lead = groups[1] or ''
        return '<a href="http://{1}{2}" rel="nofollow" target="_blank">{0}{1}{2}</a>{3}{4}'.format(protocol, www_lead, *groups[2:])
    return _link.sub(replace, text)

def _format_datetime(dt):
    local_dt = dt.replace(tzinfo=pytz.utc).astimezone(_local_tz)
    return _local_tz.normalize(local_dt).strftime('%Y-%m-%d %H:%M')

def _escape_html_character(text):
    return text.replace('&', '&amp;').replace('>', '&gt;').replace('"', '&quot;').replace("'", '&#39;')

def _format_comment(comment):
    replaced = _escape_html_character(comment).replace('<', '&lt;').replace('>', '&gt;')
    return _convert_links(replaced).strip().replace('\n', '<br>')

def _remove_query_string(referer):
    return referer.split('?')[0]

def _add_ticket_id(referer, ticket_id):
    address = _remove_query_string(referer)
    if ticket_id > _Invalid_ticket_id:
        address += '?ticket_id=' + str(ticket_id)
    return address

def _burn_up_data(project_id):
    back_days = 23
    future_days = 2
    first_day = _additional_date(-back_days)

    if project_id == _All:
        workloads = DBSession.query(Dailywork).filter(Dailywork.date >= first_day).all()
        workload = DBSession.query(Dailywork).filter(Dailywork.date < first_day).order_by(Dailywork.date.desc()).first()

    else:
        workloads = DBSession.query(Workload).filter(Workload.project_id == project_id, Workload.date >= first_day).all()
        workload = DBSession.query(Workload).filter(Workload.project_id == project_id, Workload.date < first_day).order_by(Workload.date.desc()).first()

    chart_data = []
    day = first_day
    today = _additional_date()
    is_dailywork = True if project_id == _All else False
    for i in range(back_days + 1 + future_days):
        chart_data.append({'date': _date_only_str(day),
                           'day': day.day, 'weekday': day.weekday(),
                           'total': 0, 'completed': 0,
                           })

        # day に配置する workload の更新
        if len(workloads) > 0 and day == workloads[0].date:
            workload = workloads.pop(0)

        # day に workload を配置する
        if workload:
            chart_data[i]['completed'] = workload.completed
            if is_dailywork:
                chart_data[i]['total'] = workload.opened
            else:
                chart_data[i]['total'] = workload.total
        else:
            if is_dailywork:
                chart_data[i]['completed'] = 0
                chart_data[i]['total'] = 0

        # 今日以降のデータは空にする
        if day == today or project_id == _All:
            workload = None

        day += timedelta(days=1)

    return chart_data

def _additional_date(add_days=0):
    return datetime.combine(date.today() + timedelta(days=add_days),
                            datetime.min.time())

def _additional_datetime(add_hours=0, add_minutes=0):
    return datetime.utcnow() + timedelta(hours=add_hours, minutes=add_minutes)

def _date_only_str(day):
    return str(day).split(' ')[0]

@view_config(route_name='all_projects',
             renderer='templates/tickets.pt', permission='edit')
def all_projects(request):
    request._LOCALE_ = _locale_name()
    setting = DBSession.query(Setting).one()
    show_ticket = request.GET.get('ticket_id')
    return _tickets_data(request, show_ticket)

@view_config(route_name='single_project',
             renderer='templates/tickets.pt', permission='edit')
def single_project(request):
    request._LOCALE_ = _locale_name()
    show_ticket = request.GET.get('ticket_id')
    show_project = request.matchdict.get('show_project', _All)
    return _tickets_data(request, show_ticket, show_project)

def _tickets_data(request, show_ticket, show_project=_All):
    show_resolved = _show_resolved(request.session, show_project)
    show_mascot = _is_show_mascot(request)

    show_project_category = request.session.get('show_project_category', None)
    try:
        # プロジェクト情報の取得
        all_projects = DBSession.query(Project).filter(Project.active==True, or_(Project.category_id==show_project_category, Project.category_id==None, show_project_category==None)).order_by(Project.order).all()
        if show_project == _All:
            projects = all_projects[:]
            chart_data = _burn_up_data(_All)
            chart_options = {'today': _date_only_str(_additional_date())}
        else:
            projects = DBSession.query(Project).filter(Project.active==True, Project.id==int(show_project), or_(Project.category_id==show_project_category, Project.category_id==None, show_project_category==None)).all()
            chart_data = _burn_up_data(int(show_project))
            chart_options = {'today': _date_only_str(_additional_date())}

        # プロジェクトに所属しているチケット情報の取得
        tickets = {}
        for project in projects:
            show_recent_ticket = False if show_project == _All else True
            tickets[project.id] = _project_ticket(project.id, show_resolved, show_recent_ticket)

        # スター付きチケットがあれば、それを最初に表示する
        if show_project == _All:
            stared_tickets = _stared_tickets()
            if len(stared_tickets) > 0:
                tickets[0] = stared_tickets
                star_project = Project(id=0, name='star tickets')
                projects = [star_project] + all_projects[:]

        # 有効なチケットがないプロジェクトは最後に表示させる
        project_index = 0
        for i in range(len(projects)):
            project_tickets = tickets[projects[project_index].id]
            if len(project_tickets) == 0:
                # チケットが存在しない
                _push_to_last(projects, project_index)

            elif project_tickets[0].priority_id == Priority.Trivial:
                # Trivial チケットしかない
                _push_to_last(projects, project_index)

            elif project_tickets[0].state_id != State.Open:
                # Resolved, Rejected チケットしかない
                _push_to_last(projects, project_index)

            else:
                project_index += 1

        project_categories = DBSession.query(ProjectCategory).filter(ProjectCategory.active==True).order_by(ProjectCategory.name).all()

        return {
            'show_mascot': show_mascot,
            'Priority': Priority.Labels,
            'State': State.Labels,
            'all_projects': all_projects,
            'project_categories': project_categories,
            'show_project_category': show_project_category,
            'projects': projects,
            'tickets': tickets,
            'show_project': show_project,
            'show_ticket': show_ticket,
            'burn_up_data': chart_data,
            'burn_up_options': chart_options,
            'show_resolved': show_resolved,
        }

    except DBAPIError:
        return Response(conn_err_msg, content_type='text/plain', status_int=500)

conn_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_query_manager_db" script
    to initialize your database tables.  Check your virtual
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""


def _show_resolved(session, show_project):
    show_resolved = True if session.get('show_resolved') else False
    if show_project == _All:
        show_resolved = False
    return show_resolved

def _project_ticket(project_id, show_resolved, show_recent_ticket):
    recent_minutes = -10 if show_recent_ticket else +1
    recent_datetime = _additional_datetime(0, recent_minutes)
    return DBSession.query(Ticket).filter(Ticket.project_id==project_id, or_(show_resolved, Ticket.state_id==1, Ticket.updated_at > recent_datetime)).order_by(Ticket.state_id, Ticket.priority_id, Ticket.updated_at.desc()).all()

def _stared_tickets():
    return DBSession.query(Ticket).join(Project).filter(Ticket.star_checked==True, Ticket.state_id==1, Project.active==True).order_by(Ticket.state_id, Ticket.priority_id, Ticket.updated_at.desc()).all()

def _is_show_mascot(request):
    return True if request.session.get('show_mascot') != False else False

def _push_to_last(projects, project_index):
    projects.append(projects.pop(project_index))

@view_config(route_name='edit_ticket', renderer='json')
def edit_ticket(request):
    # 指定されたチケット情報の取得
    ticket_id = int(request.matchdict.get('ticket_id'))
    return _edit_ticket_response(request, ticket_id)

def _edit_ticket_response(request, ticket_id):
    ticket = DBSession.query(Ticket).filter(Ticket.id==ticket_id).one_or_none()
    if not ticket:
        return HTTPFound(location=request.referer)

    request._LOCALE_ = _locale_name()
    localizer = request.localizer
    show_mascot = _is_show_mascot(request)
    show_project = request.matchdict.get('show_project', _All)

    # チケット移動フォーム用のプロジェクト情報の登録
    projects = []
    for project in DBSession.query(Project).filter(Project.active==True).order_by(Project.order):
        projects.append({'id': project.id, 'name': project.name})

    comments = []
    for comment in DBSession.query(Comment).filter(Comment.ticket_id==ticket_id):
        comments.append({'datetime': _format_datetime(comment.created_at),
                         'comment': _format_comment(comment.comment),
                         'id': comment.id,
                         })

    return {
        'show_mascot': show_mascot,
        'show_project' : show_project,
        'project_name': ticket.project.name,
        'project_id': ticket.project_id,
        'projects': projects,
        'ticket_id': ticket_id,
        'star_checked': ticket.star_checked,
        'created_at': _format_datetime(ticket.created_at),
        'title': _escape_html_character(ticket.title),
        'state_id': ticket.state_id,
        'priority_id': ticket.priority_id,
        'comments': comments,
        'base_address': _remove_query_string(request.referer),

        '_Move_to': localizer.translate(_('move to "{0}"')),
        '_Ticket': localizer.translate(_('Ticket')),
        '_Created': localizer.translate(_('Created:')),
        '_Title': localizer.translate(_('Title')),
        '_Remove_this_comment': localizer.translate(_('remove this comment.')),
        '_Comment': localizer.translate(_('Comment')),
        '_State': localizer.translate(_('State')),
        '_Priority': localizer.translate(_('Priority')),
        '_Update': localizer.translate(_('Update')),
    }

@view_config(route_name='add_ticket', renderer='json')
def add_ticket(request):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    project_id = int(request.matchdict.get('project_id'))
    show_project = request.matchdict.get('show_project', _All)
    project = DBSession.query(Project).filter(Project.id==project_id).one()

    return {
        'show_mascot': show_mascot,
        'project_id': project_id,
        'project_name': project.name,
        'show_project' : show_project,
        'base_address': _remove_query_string(request.referer),

        '_New_ticket': localizer.translate(_('New ticket')),
        '_Title': localizer.translate(_('Title')),
        '_Comment': localizer.translate(_('Comment')),
        '_State': localizer.translate(_('State')),
        '_Priority': localizer.translate(_('Priority')),
        '_Create': localizer.translate(_('Create')),
    }

@view_config(route_name='create_ticket', renderer='json')
def create_ticket(request):
    created_ticket = _add_new_ticket(request)
    if not created_ticket:
        return {}

    _puppeteer().ticket_created()
    show_project = request.POST.get('show_project')
    show_resolved = _show_resolved(request.session, show_project)
    return _project_tickets_json(created_ticket, show_project, show_resolved)

def _add_new_ticket(request):
    title = request.POST.get('title').strip()
    if not title:
        # タイトルが空の場合は登録しない
        return None

    state_id = int(request.POST.get('state'))
    priority_id = int(request.POST.get('priority'))
    project_id = int(request.POST.get('project_id'))

    # １秒以内に作られた直前のチケットと全く同じ内容ならば作成しない
    previous = DBSession.query(Ticket.title, Ticket.state_id, Ticket.priority_id, Ticket.project_id, func.max(Ticket.id)).filter(Ticket.created_at >= (datetime.utcnow() - timedelta(seconds=1))).one_or_none()
    if previous:
        if title == previous.title and state_id == previous.state_id and priority_id == previous.priority_id and project_id == previous.project_id:
            return None

    #DBSession.query(Ticket.id).with_lockmode('update').limit(1).all()
    ticket_id = DBSession.query(func.max(Ticket.id)+1).scalar()

    ticket = Ticket(title=title,
                    state_id=state_id, priority_id=priority_id,
                    project_id=project_id)
    DBSession.add(ticket)

    comment = request.POST.get('comment', '').strip()
    if comment:
        DBSession.add(Comment(comment=comment, ticket_id=ticket_id))

    _add_to_workload(ticket)
    _add_to_dailywork(ticket)

    return ticket

def _project_tickets_json(this_ticket, show_project, show_resolved):
    show_recent_ticket = False if show_project == _All else True
    project_tickets = _project_ticket(this_ticket.project_id, show_resolved, show_recent_ticket)
    tickets = []
    for ticket in project_tickets:
        tickets.append(_ticket_json(ticket))

    star_tickets = []
    for ticket in _stared_tickets():
        star_tickets.append(_ticket_json(ticket))

    return {
        'star_tickets': star_tickets,

        'project_id': this_ticket.project_id,
        'created_ticket_id': this_ticket.id,
        'tickets': tickets,
        'show_project': show_project,

        'burn_up_data': _burn_up_data(show_project),
        'burn_up_options': {'today': _date_only_str(_additional_date())},
    }

def _ticket_json(ticket):
    return {
        'id': int(ticket.id),
        'title': ticket.title,
        'star_checked': ticket.star_checked,
        'state_id': int(ticket.state_id),
        'priority_id': int(ticket.priority_id),
    }

@view_config(route_name='update_ticket', renderer='json')
def update_ticket(request):
    updated_ticket = _update_ticket(request)
    if not updated_ticket:
        return {}

    show_project = request.POST.get('show_project')
    show_resolved = _show_resolved(request.session, show_project)
    return _project_tickets_json(updated_ticket, show_project, show_resolved)

def _update_ticket(request):
    title = request.POST.get('title').strip()
    if not title:
        # タイトルが空の場合はデータを更新しない
        return None

    ticket_id = int(request.POST.get('ticket_id'))
    ticket = DBSession.query(Ticket).filter(Ticket.id==ticket_id).one()
    _remove_from_workload(ticket)
    _before_update_dailywork(ticket)

    state_id = int(request.POST.get('state'))
    priority_id = int(request.POST.get('priority'))

    title_updated = ticket.title != title
    state_updated = ticket.state_id != state_id
    priority_updated = ticket.priority_id != priority_id
    comment_added = False

    ticket.title = title
    ticket.state_id = state_id
    ticket.priority_id = priority_id
    ticket.updated_at = datetime.utcnow()

    comment = request.POST.get('comment')
    if comment:
        DBSession.add(Comment(comment=comment, ticket_id=ticket_id))
        comment_added = True

    _add_to_workload(ticket)
    _after_update_dailywork(ticket)

    if state_updated:
        _puppeteer().state_updated(state_id)
    elif priority_updated:
        _puppeteer().priority_updated(priority_id)
    elif title_updated:
        _puppeteer().title_updated()
    elif comment_added:
        _puppeteer().comment_added()

    return ticket

@view_config(route_name='update_ticket_priority', renderer='json')
def update_ticket_priority(request):
    ticket_id = int(request.matchdict.get('ticket_id'))
    priority_id = int(request.matchdict.get('priority_id'))
    show_project = request.matchdict.get('show_project')
    updated_ticket = _change_ticket_priority(ticket_id, priority_id)
    _puppeteer().priority_updated(priority_id)

    show_resolved = _show_resolved(request.session, show_project)
    return _project_tickets_json(updated_ticket, show_project, show_resolved)

def _change_ticket_priority(ticket_id, priority_id):
    ticket = DBSession.query(Ticket).filter(Ticket.id==ticket_id).one()
    _remove_from_workload(ticket)
    ticket.priority_id = priority_id
    ticket.updated_at = datetime.utcnow()
    _add_to_workload(ticket)
    return ticket

@view_config(route_name='toggle_ticket_star', renderer='json')
def toggle_ticket_star(request):
    ticket_id = int(request.matchdict.get('ticket_id'))
    updated_ticket = _toggle_ticket_star(ticket_id)
    show_project = request.matchdict.get('show_project', _All)
    show_resolved = _show_resolved(request.session, show_project)
    return _project_tickets_json(updated_ticket, show_project, show_resolved)

def _toggle_ticket_star(ticket_id):
    ticket = DBSession.query(Ticket).filter(Ticket.id==ticket_id).one()
    ticket.star_checked = not ticket.star_checked
    return ticket

@view_config(route_name='move_ticket', renderer='json')
def move_ticket(request):
    ticket_id = int(request.matchdict.get('ticket_id'))
    project_id = request.matchdict.get('project_id')

    ticket = DBSession.query(Ticket).filter(Ticket.id==ticket_id).one()
    _remove_from_workload(ticket)
    ticket.project_id = project_id
    _add_to_workload(ticket)

    address = _add_ticket_id(request.referer, ticket_id)
    return HTTPFound(location=address)

@view_config(route_name='delete_comment', renderer='json')
def delete_comment(request):
    comment_id = int(request.matchdict.get('comment_id'))
    comment = DBSession.query(Comment).filter(Comment.id==comment_id).one()
    ticket_id = comment.ticket_id
    DBSession.delete(comment)

    return _edit_ticket_response(request, ticket_id)

@view_config(route_name='show_resolved_ticket', renderer='templates/tickets.pt')
def show_resolved_ticket(request):
    session = request.session
    session['show_resolved'] = request.POST.get('show_resolved', False)
    session.changed()

    return HTTPFound(location=request.referer)

@view_config(route_name='set_project_category', renderer='templates/tickets.pt')
def set_project_category(request):
    value = request.POST.get('show_project_category', None)
    category_id = int(value) if value and int(value) >= 0 else None
    session = request.session
    session['show_project_category'] = category_id
    session.changed()

    return HTTPFound(location=request.referer)



@view_config(route_name='configure_project',
             renderer='templates/projects.pt', permission='edit')
def configure_project(request):
    return _configure_project(request)

@view_config(route_name='configure_project_add',
             renderer='templates/projects.pt')
def configure_project_add(request):
    return _configure_project(request, 'add')

def _configure_project(request, mode=''):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    active_projects = DBSession.query(Project).filter(Project.active==True).order_by(Project.order).all()
    removed_projects = DBSession.query(Project).filter(Project.active==False).order_by(Project.order).all()

    return {
        'show_mascot': show_mascot,
        'config': 'projects',
        'mode': mode,
        'active_projects': active_projects,
        'removed_projects': removed_projects,
        '_Want_to_remove': localizer.translate(_('Want to remove "{0}"?')),
        '_Want_to_restore': localizer.translate(_('Want to restore "{0}"?')),
    }

@view_config(route_name='save_project_order', renderer='templates/projects.pt')
def save_project_order(request):
    order = request.matchdict.get('orders')

    order_index = 1
    for project_id in order.split(','):
        project = DBSession.query(Project).filter(Project.id==int(project_id)).one()
        project.order = order_index
        order_index += 1

    return HTTPFound(location=request.referer)

@view_config(route_name='add_project', renderer='json')
def add_project(request):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    return {
        'base_address': request.referer,
        'show_mascot': show_mascot,
        '_Create_new_project': localizer.translate(_('Create new project')),
        '_Project_name': localizer.translate(_('Project name')),
        '_Create': localizer.translate(_('Create')),
    }

@view_config(route_name='create_project', renderer='templates/projects.pt')
def create_project(request):
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    _all_project_order_increment()
    _add_new_project(request)

    _puppeteer().project_created()
    return HTTPFound(location=request.referer)

def _add_new_project(request):
    name = request.POST.get('name', None)
    if not name:
        # 名前のないプロジェクトは登録しない
        return

    DBSession.add(Project(name=name))

@view_config(route_name='edit_project', renderer='json')
def edit_project(request):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    # 指定されたプロジェクト情報の取得
    project_id = request.matchdict.get('project_id')

    project = DBSession.query(Project).filter(Project.id==project_id).one_or_none()
    if not project:
        return HTTPFound(location=request.referer)

    return {
        'base_address': request.referer,
        'show_mascot': show_mascot,
        'project_id': project.id,
        'project_name': project.name,
        '_Project_name': localizer.translate(_('Project name')),
        '_Edit_project_name': localizer.translate(_('Edit project name')),
        '_Update': localizer.translate(_('Update')),
    }

@view_config(route_name='update_project', renderer='templates/projects.pt')
def update_project(request):
    name = request.POST.get('name').strip()
    if not name:
        # プロジェクト名が空の場合はデータを更新しない
        return HTTPFound(location=request.referer)

    project_id = request.POST.get('project_id')
    project = DBSession.query(Project).filter(Project.id==project_id).one()
    project.name = name

    _puppeteer().project_updated()
    return HTTPFound(location=request.referer)

@view_config(route_name='remove_project', renderer='templates/projects.pt')
def remove_project(request):
    project_id = request.matchdict.get('project_id')
    _set_project_active(project_id, False)

    _puppeteer().project_removed()
    return HTTPFound(location=request.referer)

@view_config(route_name='restore_project', renderer='templates/projects.pt')
def restore_project(request):
    project_id = request.matchdict.get('project_id')
    _set_project_active(project_id, True)

    _puppeteer().project_restored()
    return HTTPFound(location=request.referer)

def _set_project_active(project_id, active):
    project = DBSession.query(Project).filter(Project.id==project_id).one()
    project.active = active
    if active:
        project.order = DBSession.query(func.max(Project.order)+1).scalar()
    else:
        _all_project_order_increment()
        project.order = 0

def _all_project_order_increment():
    for project in DBSession.query(Project).all():
        project.order = project.order + 1

@view_config(route_name='edit_project_category', renderer='json')
def edit_project_category(request):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    # 指定されたカテゴリ情報の取得
    project_category_id = request.matchdict.get('project_category_id')

    project_category = DBSession.query(ProjectCategory).filter(ProjectCategory.id==project_category_id).one_or_none()
    if not project_category:
        return HTTPFound(location=request.referer)

    return {
        'base_address': request.referer,
        'show_mascot': show_mascot,
        'project_category_id': project_category.id,
        'project_category_name': project_category.name,
        '_Project_category_name': localizer.translate(_('Project category name')),
        '_Edit_project_category_name': localizer.translate(_('Edit project category name')),
        '_Update': localizer.translate(_('Update')),
    }

@view_config(route_name='update_project_category', renderer='templates/project_category.pt')
def update_project_category(request):
    name = request.POST.get('name').strip()
    if not name:
        # カテゴリ名が空の場合はデータを更新しない
        return HTTPFound(location=request.referer)

    project_category_id = request.POST.get('project_category_id')
    project_category = DBSession.query(ProjectCategory).filter(ProjectCategory.id==project_category_id).one()
    project_category.name = name

    return HTTPFound(location=request.referer)

def _remove_project_category(project_category_id):
    project_category = DBSession.query(ProjectCategory).filter(ProjectCategory.id==project_category_id).one()
    project_category.active = False

    DBSession.query(Project).filter(Project.category_id==project_category_id).update({Project.category_id: None})

@view_config(route_name='create_project_category', renderer='templates/project_category.pt')
def create_project_project(request):
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    _add_new_project_category(request)

    return HTTPFound(location=request.referer)

def _add_new_project_category(request):
    name = request.POST.get('name')
    if not name:
        # 名前のないカテゴリは登録しない
        return

    DBSession.add(ProjectCategory(name=name))


@view_config(route_name='configure_project_category',
             renderer='templates/project_category.pt', permission='edit')
def configure_project_category(request):
    return _configure_project_category(request)

@view_config(route_name='configure_project_category_add',
             renderer='templates/project_category.pt')
def configure_project_category_add(request):
    return _configure_project_category(request, 'add')

@view_config(route_name='configure_project_category_remove',
             renderer='templates/project_category.pt')
def configure_project_category_remove(request):
    return _configure_project_category(request, 'remove')

def _configure_project_category(request, mode=''):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    project_categories = DBSession.query(ProjectCategory).filter(ProjectCategory.active==True).order_by(ProjectCategory.name).all()

    active_projects = DBSession.query(Project).filter(Project.active==True).order_by(Project.order).all()

    return {
        'show_mascot': show_mascot,
        'config': 'project_category',
        'mode': mode,
        'project_categories': project_categories,
        'active_projects': active_projects,
        '_Want_to_remove': localizer.translate(_('Want to remove "{0}"?')),
    }

@view_config(route_name='save_project_category', renderer='templates/project_category.pt')
def save_project_category(request):
    active_projects = DBSession.query(Project).filter(Project.active==True).order_by(Project.order).all()
    for project in active_projects:
        name = 'p' + str(project.id)
        value = request.POST.get(name)
        category_id = int(value) if value and int(value) >= 0 else None
        project.category_id = category_id

    return HTTPFound(location=request.referer)

@view_config(route_name='add_project_category', renderer='json')
def add_project_category(request):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    localizer = request.localizer

    return {
        'base_address': request.referer,
        'show_mascot': show_mascot,
        '_Create_new_project_category': localizer.translate(_('Create new project category')),
        '_Project_category_name': localizer.translate(_('Project Category name')),
        '_Create': localizer.translate(_('Create')),
    }

@view_config(route_name='remove_project_category', renderer='templates/project_category.pt')
def remove_project_category(request):
    project_category_id = request.matchdict.get('project_category_id')
    _remove_project_category(project_category_id)

    return HTTPFound(location=request.referer)


@view_config(route_name='edit_setting',
             renderer='templates/user_config.pt', permission='edit')
def edit_setting(request):
    show_mascot = _is_show_mascot(request)
    request._LOCALE_ = _locale_name()
    setting = DBSession.query(Setting).one()

    return {
        'show_mascot': show_mascot,
        'config': 'user',
        'locales': _locales,
        'locale_keys': _locale_keys,
        'current_locale': setting.locale
    }

@view_config(route_name='save_setting', renderer='templates/user_config.pt')
def save_setting(request):
    setting = DBSession.query(Setting).one()
    setting.locale = request.POST.get('locale')

    global _locale
    _locale = setting.locale

    return HTTPFound(location="/setting/edit")


def _add_to_workload(ticket):
    today = _additional_date()
    _update_workload(ticket, +1, today)

def _remove_from_workload(ticket):
    today = _additional_date()
    _update_workload(ticket, -1, today)

def _update_workload(ticket, count, today):
    if ticket.priority_id >= Priority.Trivial:
        # Trivial はカウントの対象外
        return

    # 本日の Workload レコードの取得
    project_id = ticket.project_id
    today_workload = DBSession.query(Workload).filter(Workload.project_id==project_id, Workload.date==today).one_or_none()

    if today_workload:
        _add_workload_values(today_workload, ticket.state_id, count)

    else:
        # 直近の total, completed の値を取得して登録する
        previous_date = DBSession.query(func.max(Workload.date).label('max_date')).filter(Workload.project_id==project_id).one_or_none()
        if previous_date.max_date:
            previous_workload = DBSession.query(Workload).filter(Workload.project_id==project_id, Workload.date==previous_date.max_date).one_or_none()

            workload = Workload(date=today, project_id=project_id,
                                total=previous_workload.total,
                                completed=previous_workload.completed)
            _add_workload_values(workload, ticket.state_id, count)
            DBSession.add(workload)

        else:
            workload = Workload(date=today, project_id=project_id, total=0, completed=0)
            _add_workload_values(workload, ticket.state_id, count)
            DBSession.add(workload)

def _add_workload_values(workload, state_id, count):
    if state_id == State.Open:
        workload.total += count

    elif state_id == State.Finish:
        workload.total += count
        workload.completed += count

@view_config(route_name='show_mascot', renderer='templates/tickets.pt')
def show_mascot(request):
    _set_mascot_visible(request, True)
    _puppeteer().show_mascot()
    return HTTPFound(location=request.referer)

@view_config(route_name='hide_mascot', renderer='templates/tickets.pt')
def hide_mascot(request):
    _set_mascot_visible(request, False)
    _puppeteer().hide_mascot()
    return HTTPFound(location=request.referer)

def _set_mascot_visible(request, visible):
    session = request.session
    session['show_mascot'] = visible
    session.changed()


def _add_to_dailywork(ticket):
    today = _additional_date()
    _add_dailywork_opened(today)
    pass

def _before_update_dailywork(ticket):
    today = _additional_date()
    if ticket.state_id == State.Finish:
        _update_dailywork_completed(today, -1)

def _after_update_dailywork(ticket):
    today = _additional_date()
    if ticket.state_id == State.Finish:
        _update_dailywork_completed(today, +1)

def _add_dailywork_opened(today):
    _update_dailywork(today, 1, 0)

def _update_dailywork_completed(today, count):
    _update_dailywork(today, 0, count)

def _update_dailywork(today, opened, completed):
    dailywork = DBSession.query(Dailywork).filter(Dailywork.date==today).one_or_none()
    if dailywork:
        dailywork.opened += opened
        dailywork.completed += completed

    else:
        dailywork = Dailywork(date=today, opened=opened, completed=completed)
        DBSession.add(dailywork)

@view_config(route_name='login', renderer='templates/login.pt')
@forbidden_view_config(renderer='templates/login.pt')
def login(request):
    login_url = request.route_url('login')
    referrer = request.url
    if referrer == login_url:
        referrer = '/'  # never use login form itself as came_from
    came_from = request.params.get('came_from', referrer)

    # !!!
    headers = remember(request, 'owner')
    return HTTPFound(location=came_from, headers=headers)

def logout(request):
    headers = forget(request)
    url = request.route_url('all_projects')
    return HTTPFound(location=url, headers=headers)
