# -*- coding: utf-8 -*-
#import bcrypt
from .models import (
    DBSession,
    Member,
)


def hash_password(pw):
    pwhash = bcrypt.hashpw(pw.encode('utf8'), bcrypt.gensalt())
    return pwhash.decode('utf8')

def check_password(pw, hashed_pw):
    expected_hash = hashed_pw.encode('utf8')
    return bcrypt.checkpw(pw.encode('utf8'), expected_hash)


GROUPS = {'owner': ['group:owner']}


def groupfinder(userid, request):
    member = DBSession.query(Member).one_or_none()
    # Member が空のときは認証済みとして扱う
    if member == None:
        return GROUPS.get('owner')

    # check authenticated
    if check_password(request.password, member.password):
        return GROUPS.get('owner')

    return []
