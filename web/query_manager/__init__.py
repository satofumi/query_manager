from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from .security import groupfinder
from sqlalchemy import engine_from_config

from .models import (
    DBSession,
    Base,
    )

from pyramid_beaker import session_factory_from_settings


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings,
                          root_factory='.resources.Root')
    config.include('pyramid_beaker')
    config.include('pyramid_chameleon')
    config.add_translation_dirs('query_manager:locale/')
    config.add_static_view('static', 'static', cache_max_age=3600)

    # Security policies
    authn_policy = AuthTktAuthenticationPolicy(
        settings['query_manager.secret'], callback=groupfinder,
        hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)

    config.add_route('login', '/login')

    config.add_route('all_projects', '/')
    config.add_route('single_project', '/{show_project:\d+}')

    config.add_route('create_ticket', '/ticket/create')
    config.add_route('update_ticket', '/ticket/update')
    config.add_route('update_ticket_priority', '/ticket/update/priority/{ticket_id}/{priority_id}/{show_project}')
    config.add_route('toggle_ticket_star', '/ticket/toggle/star/{ticket_id}/{show_project}')
    config.add_route('edit_ticket', '/ticket/edit/{ticket_id:\d+}/{show_project}')
    config.add_route('add_ticket', '/ticket/add/{project_id:\d+}/{show_project}')
    config.add_route('move_ticket', '/ticket/move/{ticket_id:\d+}/{project_id:\d+}')
    config.add_route('delete_comment', '/comment/delete/{comment_id:\d+}')
    config.add_route('show_resolved_ticket', '/ticket/show/resolved')
    config.add_route('set_project_category', '/ticket/category/set')

    config.add_route('configure_project', '/project/configure')
    config.add_route('configure_project_add', '/project/configure/add')
    config.add_route('save_project_order', '/project/save_order/{orders}')
    config.add_route('add_project', '/project/add')
    config.add_route('create_project', '/project/create')
    config.add_route('edit_project', '/project/edit/{project_id:\d+}')
    config.add_route('update_project', '/project/update')
    config.add_route('remove_project', '/project/remove/{project_id:\d+}')
    config.add_route('restore_project', '/project/restore/{project_id:\d+}')

    config.add_route('configure_project_category', '/project_category/configure')
    config.add_route('configure_project_category_add', '/project_category/configure/add')
    config.add_route('configure_project_category_remove', '/project_category/configure/remove')
    config.add_route('save_project_category', '/project_category/save')
    config.add_route('add_project_category', '/project_category/add')
    config.add_route('create_project_category', '/project_category/create')
    config.add_route('edit_project_category', '/project_category/edit/{project_category_id:\d+}')
    config.add_route('update_project_category', '/project_category/update')
    config.add_route('remove_project_category', '/project_category/remove/{project_category_id:\d+}')

    config.add_route('edit_setting', '/setting/edit')
    config.add_route('save_setting', '/setting/save')

    config.add_route('show_mascot', '/mascot/show')
    config.add_route('hide_mascot', '/mascot/hide')

    config.scan()
    return config.make_wsgi_app()
