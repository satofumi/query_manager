# -*- coding: utf-8 -*-

import threading
import socket
import errno
from socket import error as socket_error
from .state import State
from .priority import Priority


_host = '127.0.0.1'
_port = 17821

def _send_thread(command):
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((_host, _port))
        client.send(command.encode())
        client.close()

    except socket_error as serr:
        if serr.errno != errno.ECONNREFUSED:
            raise serr


class Puppeteer:
    def __init__(self):
        pass

    def _send_command(self, command):
        thread = threading.Thread(target=_send_thread,
                                  name="send_thread", args=(command,))
        thread.start()

    def show_mascot(self):
        self._send_command('Show')

    def hide_mascot(self):
        self._send_command('Hide')

    def ticket_created(self):
        self._send_command('Ticket.State.' + State.Labels[State.Open])

    def state_updated(self, state_id):
        self._send_command('Ticket.State.' + State.Labels[state_id])

    def priority_updated(self, priority_id):
        self._send_command('Ticket.Priority.' + Priority.Labels[priority_id])

    def title_updated(self):
        self._send_command('Ticket.Title.Updated')

    def comment_added(self):
        self._send_command('Ticket.Comment.Added')

    def project_created(self):
        self._send_command('Project.Created')

    def project_removed(self):
        self._send_command('Project.Removed')

    def project_restored(self):
        self._send_command('Project.Restored')

    def project_updated(self):
        self._send_command('Project.Updated')
