class Priority:
    Critical = 1
    Major = 2
    Minor = 3
    Trivial = 4

    Labels = [None, 'Critical', 'Major', 'Minor', 'Trivial']
