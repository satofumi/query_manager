import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'pytz',
    'tzlocal == 2.1',
    'Babel',
    'lingua',
    'pyramid_beaker',
    'alembic',
    'bcrypt',
    ]

setup(name='query_manager',
      version='0.0',
      description='query_manager',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='satofumi@users.sourceforge.jp',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='query_manager',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = query_manager:main
      [console_scripts]
      initialize_query_manager_db = query_manager.scripts.initializedb:main
      """,
      )
