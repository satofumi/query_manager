include package_version.mk

DIST_DIR = $(PACKAGE_DIR)
WEB_DIR = web

QM_DIR = $(WEB_DIR)/query_manager
SCRIPTS_DIR = $(QM_DIR)/scripts
LOCALE_JA_DIR = $(QM_DIR)/locale/ja/LC_MESSAGES/
STATIC_DIR = $(QM_DIR)/static
CSS_DIR = $(STATIC_DIR)/css
FONTS_DIR = $(STATIC_DIR)/fonts
TEMPLATES_DIR = $(QM_DIR)/templates
ALEMBIC_DIR = $(WEB_DIR)/alembic
ALEMBIC_VERSIONS_DIR = $(ALEMBIC_DIR)/versions


WEB_FILES = \
	$(WEB_DIR)/production.ini \
	$(WEB_DIR)/setup.py \
	$(WEB_DIR)/README.txt \
	$(WEB_DIR)/CHANGES.txt \
	$(WEB_DIR)/alembic.ini \

QM_FILES = \
	$(QM_DIR)/*.py \

SCRIPTS_FILES = \
	$(SCRIPTS_DIR)/*.py \

TEMPLATES_FILES = \
	$(TEMPLATES_DIR)/*.pt \

STATIC_FILES = \
	$(STATIC_DIR)/*.js \
	$(STATIC_DIR)/*.png \
	$(STATIC_DIR)/*.css \

CSS_FILES = \
	$(CSS_DIR)/*.css \

FONTS_FILES = \
	$(FONTS_DIR)/*.* \

LOCALE_JA_FILES = \
	$(LOCALE_JA_DIR)/query_manager.mo \

ALEMBIC_FILES = \
	$(ALEMBIC_DIR)/README \
	$(ALEMBIC_DIR)/env.py \
	$(ALEMBIC_DIR)/script.py.mako \

ALEMBIC_VERSIONS_FILES = \
	$(ALEMBIC_VERSIONS_DIR)/*.py \

all :
	-msgfmt -o $(LOCALE_JA_DIR)/query_manager.mo $(LOCALE_JA_DIR)/query_manager.po
	\
	mkdir -p $(DIST_DIR)/$(SCRIPTS_DIR)
	mkdir -p $(DIST_DIR)/$(TEMPLATES_DIR)
	mkdir -p $(DIST_DIR)/$(STATIC_DIR)
	mkdir -p $(DIST_DIR)/$(CSS_DIR)
	mkdir -p $(DIST_DIR)/$(FONTS_DIR)
	mkdir -p $(DIST_DIR)/$(LOCALE_JA_DIR)
	mkdir -p $(DIST_DIR)/$(ALEMBIC_DIR)
	mkdir -p $(DIST_DIR)/$(ALEMBIC_VERSIONS_DIR)
	cp $(WEB_FILES) $(DIST_DIR)/$(WEB_DIR)
	cp $(QM_FILES) $(DIST_DIR)/$(QM_DIR)
	cp $(SCRIPTS_FILES) $(DIST_DIR)/$(SCRIPTS_DIR)
	cp $(TEMPLATES_FILES) $(DIST_DIR)/$(TEMPLATES_DIR)
	cp $(STATIC_FILES) $(DIST_DIR)/$(STATIC_DIR)
	cp $(CSS_FILES) $(DIST_DIR)/$(CSS_DIR)
	cp $(FONTS_FILES) $(DIST_DIR)/$(FONTS_DIR)
	-cp $(LOCALE_JA_FILES) $(DIST_DIR)/$(LOCALE_JA_DIR)
	cp $(ALEMBIC_FILES) $(DIST_DIR)/$(ALEMBIC_DIR)
	cp $(ALEMBIC_VERSIONS_FILES) $(DIST_DIR)/$(ALEMBIC_VERSIONS_DIR)
