/*!
  \file
  \brief 「クエリちゃんのタスク管理」用のマスコット
*/

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include "Puppeteer.h"


int main(int argc, char *argv[])
{
    enum { Timeout_msec = 100 };
    if (Puppeteer::send_show_command(Timeout_msec)) {
        // 既存の puppeteer を Show して終了する
        return 0;
    }

    QApplication app(argc, argv);

    // ロケールの適用
    QString locale = QLocale::system().name();
    QTranslator translator;
    translator.load("puppeteer_" + locale);
    app.installTranslator(&translator);

    Puppeteer widget;

    return app.exec();
}
