﻿/*!
  \file
  \brief zip ファイルの処理

  \todo エラーコードをマクロに変更する。
*/

#include <sys/stat.h>
#include <cstring>
#include <ctime>
#include <map>
#include <sstream>
#include <fstream>
#include <zlib.h>
#include "Zip.h"

using namespace std;


#define READ_BYTES(dest, fin, byte_size) \
    read_bytes(reinterpret_cast<char*>(&dest), fin, byte_size)

#define WRITE_BYTES(fout, src, byte_size) \
    write_bytes(fout, reinterpret_cast<const char*>(&src), byte_size)


namespace
{
    enum {
        Compress_buffer_size = 16 * 1024, // 16 [KB]
    };

    const bool Finish = true;

    typedef struct
    {
        int32_t headerpos;
        uint32_t data_size;
    } zipped_data_t;

    typedef map<string, zipped_data_t> Zipped_data;
    typedef vector<string> File_names;

    typedef struct
    {
        uint32_t signature;
        uint16_t needver;
        uint16_t option;
        uint16_t comptype;
        uint16_t filetime;
        uint16_t filedate;
        uint32_t crc32;
        uint32_t compsize;
        uint32_t uncompsize;
        uint16_t fnamelen;
        uint16_t extralen;
    } zip_header_t;

    typedef struct
    {
        uint32_t signature;
        uint16_t madever;
        uint16_t needver;
        uint16_t option;
        uint16_t comptype;
        uint16_t filetime;
        uint16_t filedate;
        uint32_t crc32;
        uint32_t compsize;
        uint32_t uncompsize;
        uint16_t fnamelen;
        uint16_t extralen;
        uint16_t commentlen;
        uint16_t disknum;
        uint16_t inattr;
        uint32_t outattr;
        uint32_t headerpos;
    } central_header_t;

    typedef struct {
        string name;
        central_header_t header;
        long position;
    } central_t;
    typedef vector<central_t> Central_headers;

    typedef struct
    {
        uint32_t signature;
        uint16_t disknum;
        uint16_t startdisknum;
        uint16_t diskdirentry;
        uint16_t direntry;
        uint32_t dirsize;
        uint32_t startpos;
        uint16_t commentlen;
    } end_header_t;

    void read_bytes(char* dest, fstream* fin, int byte_size)
    {
        fin->read(dest, byte_size);
    }

    void write_bytes(fstream* fout, const char* src, int byte_size)
    {
        fout->write(src, byte_size);
    }

    void read_zip_header(zip_header_t& header, fstream* fin)
    {
        memset(&header, 0, sizeof(header));
        READ_BYTES(header.signature, fin, 4);
        READ_BYTES(header.needver, fin, 2);
        READ_BYTES(header.option, fin, 2);
        READ_BYTES(header.comptype, fin, 2);
        READ_BYTES(header.filetime, fin, 2);
        READ_BYTES(header.filedate, fin, 2);
        READ_BYTES(header.crc32, fin, 4);
        READ_BYTES(header.compsize, fin, 4);
        READ_BYTES(header.uncompsize, fin, 4);
        READ_BYTES(header.fnamelen, fin, 2);
        READ_BYTES(header.extralen, fin, 2);
    }

    void read_end_header(end_header_t& header, fstream* fin)
    {
        memset(&header, 0, sizeof(header));
        READ_BYTES(header.signature, fin, 4);
        READ_BYTES(header.disknum, fin, 2);
        READ_BYTES(header.startdisknum, fin, 2);
        READ_BYTES(header.diskdirentry, fin, 2);
        READ_BYTES(header.direntry, fin, 2);
        READ_BYTES(header.dirsize, fin, 4);
        READ_BYTES(header.startpos, fin, 4);
        READ_BYTES(header.commentlen, fin, 2);
    }

    void read_central_header(central_header_t& header, fstream* fin)
    {
        memset(&header, 0, sizeof(header));
        READ_BYTES(header.signature, fin, 4);
        READ_BYTES(header.madever, fin, 2);
        READ_BYTES(header.needver, fin, 2);
        READ_BYTES(header.option, fin, 2);
        READ_BYTES(header.comptype, fin, 2);
        READ_BYTES(header.filetime, fin, 2);
        READ_BYTES(header.filedate, fin, 2);
        READ_BYTES(header.crc32, fin, 4);
        READ_BYTES(header.compsize, fin, 4);
        READ_BYTES(header.uncompsize, fin, 4);
        READ_BYTES(header.fnamelen, fin, 2);
        READ_BYTES(header.extralen, fin, 2);
        READ_BYTES(header.commentlen, fin, 2);
        READ_BYTES(header.disknum, fin, 2);
        READ_BYTES(header.inattr, fin, 2);
        READ_BYTES(header.outattr, fin, 4);
        READ_BYTES(header.headerpos, fin, 4);
    }

    time_t unix_time(uint16_t filedate, uint16_t filetime)
    {
        int day = filedate & 0x1f;
        int month = (filedate >> 5) & 0xf;
        int year = (filedate >> 9) + 1980;

        int second = (filetime & 0x1f) * 2;
        int minute = (filetime >> 5) & 0x3f;
        int hour = filetime >> 11;

        struct tm tm;
        tm.tm_sec = second;
        tm.tm_min = minute;
        tm.tm_hour = hour;
        tm.tm_mday = day;
        tm.tm_mon = month - 1;
        tm.tm_year = year - 1900;
        tm.tm_isdst = -1;

        return mktime(&tm);
    }

    uint16_t dos_date(time_t time)
    {
        struct tm* tm = localtime(&time);
        uint16_t date_bits = 0x0000;

        date_bits |= (tm->tm_year + 1900) - 1980;
        date_bits <<= 4;

        date_bits |= tm->tm_mon + 1;
        date_bits <<= 5;

        date_bits |= tm->tm_mday;

        return date_bits;
    }

    uint16_t dos_time(time_t time)
    {
        struct tm* tm = localtime(&time);
        uint16_t time_bits = 0x0000;

        time_bits |= tm->tm_hour;
        time_bits <<= 6;

        time_bits |= tm->tm_min;
        time_bits <<= 5;

        time_bits |= tm->tm_sec / 2;

        return time_bits;
    }

    Zip::Headers read_central_headers(Zipped_data& zipped_data, fstream* fin)
    {
        Zip::Headers headers;

        fin->seekg(0, fstream::end);
        long end_position = static_cast<long>(fin->tellg());
        fin->clear();

        enum { End_header_size = 22 };
        if (end_position < End_header_size) {
            return headers;
        }

        fin->seekg(end_position - End_header_size);
        end_header_t header;
        read_end_header(header, fin);

        if (header.signature != 0x06054b50) {
            return headers;
        }

        // 順に central header を読み出す
        fin->seekg(header.startpos, fstream::beg);
        do {
            central_header_t central_header;
            read_central_header(central_header, fin);
            if ((central_header.signature & 0xffff) != 0x4b50) {
                break;
            }

            // ファイル名の読み出し
            Zip::header_t header;
            for (int i = 0; i < central_header.fnamelen; ++i) {
                char ch;
                fin->read(&ch, 1);
                header.name.push_back(ch);
            }
            if (header.name.empty()) {
                break;
            }
            header.date =
                unix_time(central_header.filedate, central_header.filetime);
            headers.push_back(header);

            // extralen ぶんのデータを無視する
            fin->seekg(central_header.extralen, fstream::cur);

            enum { Header_size = 4 + 2*5 + 4*3 + 2*2 };
            zipped_data_t data;
            data.headerpos = central_header.headerpos;
            data.data_size =
                Header_size + central_header.fnamelen +
                central_header.extralen + central_header.compsize;
            zipped_data[header.name] = data;
        } while (fin->tellg() < (end_position - End_header_size));

        return headers;
    }

    void write_central_headers(fstream* fout, Central_headers& headers)
    {
        for (Central_headers::iterator it = headers.begin();
             it != headers.end(); ++it) {
            const central_header_t& header = it->header;

            it->position = static_cast<long>(fout->tellg());

            WRITE_BYTES(fout, header.signature, 4);
            WRITE_BYTES(fout, header.madever, 2);
            WRITE_BYTES(fout, header.needver, 2);
            WRITE_BYTES(fout, header.option, 2);
            WRITE_BYTES(fout, header.comptype, 2);
            WRITE_BYTES(fout, header.filetime, 2);
            WRITE_BYTES(fout, header.filedate, 2);
            WRITE_BYTES(fout, header.crc32, 4);
            WRITE_BYTES(fout, header.compsize, 4);
            WRITE_BYTES(fout, header.uncompsize, 4);
            WRITE_BYTES(fout, header.fnamelen, 2);
            WRITE_BYTES(fout, header.extralen, 2);
            WRITE_BYTES(fout, header.commentlen, 2);
            WRITE_BYTES(fout, header.disknum, 2);
            WRITE_BYTES(fout, header.inattr, 2);
            WRITE_BYTES(fout, header.outattr, 4);
            WRITE_BYTES(fout, header.headerpos, 4);

            // 名前を書き込む
            fout->write(it->name.c_str(), it->name.size());
        }
    }

    void write_end_header(fstream* fout, const Central_headers& headers)
    {
        end_header_t header;
        header.signature = 0x06054b50;
        header.disknum = 0;
        header.startdisknum = 0;
        header.diskdirentry = headers.size();
        header.direntry = headers.size();

        const central_t& first = headers.front();
        long dirsize = static_cast<long>(fout->tellg()) - first.position;
        header.dirsize = dirsize;
        header.startpos = first.position;
        header.commentlen = 0;

        WRITE_BYTES(fout, header.signature, 4);
        WRITE_BYTES(fout, header.disknum, 2);
        WRITE_BYTES(fout, header.startdisknum, 2);
        WRITE_BYTES(fout, header.diskdirentry, 2);
        WRITE_BYTES(fout, header.direntry, 2);
        WRITE_BYTES(fout, header.dirsize, 4);
        WRITE_BYTES(fout, header.startpos, 4);
        WRITE_BYTES(fout, header.commentlen, 2);
    }

    string dummy_name(const Zipped_data& zipped_data)
    {
        int index = zipped_data.size();

        while (1) {
            stringstream ss;
            ss << "dummy_" << index;
            const string name = ss.str();
            Zipped_data::const_iterator it = zipped_data.find(name);
            if (it == zipped_data.end()) {
                return name;
            }
            ++index;
        }
    }

    class Zip_access
    {
    public:
        virtual ~Zip_access()
        {
        }

        virtual bool is_open() const = 0;
        virtual void close() = 0;
        virtual int write(const char* data, int data_size) = 0;
        virtual int read(char* data, int max_data_size) = 0;
        virtual bool eof() const = 0;
        virtual Zip::Headers headers() const = 0;
        virtual bool add_write_header(const string& name, time_t time) = 0;
        virtual bool open_read_header(const string& name) = 0;
    };


    class Zip_plain : public Zip_access
    {
    public:
        Zip_plain(const string& file_name, const string& mode)
            : is_open_(false), fio_(NULL), open_mode_(ios_base::trunc),
              left_data_size_(0)
        {
            if (mode == "w") {
                open_mode_ = ios_base::out;

            } else if (mode == "r") {
                open_mode_ = ios_base::in;

            } else {
                return;
            }

            zs_.zfree = Z_NULL;
            zs_.zalloc = Z_NULL;
            zs_.opaque = Z_NULL;

            if (open_mode_ == ios_base::out) {
                if (deflateInit2(&zs_, Z_DEFAULT_COMPRESSION,
                                 Z_DEFLATED, -MAX_WBITS, min(MAX_MEM_LEVEL, 8),
                                 Z_DEFAULT_STRATEGY) != Z_OK) {
                    return;
                }
            } else {
                if (inflateInit2(&zs_, -MAX_WBITS) != Z_OK) {
                    return;
                }
            }

            fio_ = new fstream(file_name.c_str(),
                               open_mode_ | ios_base::binary);
            if (!fio_->is_open()) {
                close();
                return;
            }
            is_open_ = true;

            if (open_mode_ == ios_base::in) {
                headers_ = read_central_headers(zipped_data_, fio_);
                if (!headers_.empty()) {
                    open_read_header(headers_.front().name);
                }
            } else {
                current_header_.signature = 0x0;
            }
        }

        ~Zip_plain()
        {
            close();
        }

        bool is_open() const
        {
            return (!fio_) ? false : is_open_;
        }

        void close()
        {
            close_header();

            if (open_mode_ == ios_base::out) {
                if (!central_headers_.empty()) {
                    deflateEnd(&zs_);
                    write_central_headers(fio_, central_headers_);
                    write_end_header(fio_, central_headers_);
                }
            } else {
                inflateEnd(&zs_);
            }

            delete fio_;
            fio_ = NULL;

            is_open_ = false;
        }

        int write(const char* data, int data_size)
        {
            if (open_mode_ != ios_base::out) {
                return -1;
            }
            if (!is_open()) {
                return -2;
            }

            if (current_header_.signature == 0x0) {
                add_write_header(dummy_name(zipped_data_), time(NULL));
            }

            // バッファにデータを格納する
            current_header_.uncompsize += data_size;
            buffer_.insert(buffer_.end(), data, data + data_size);

            // CRC を更新する
            current_header_.crc32 =
                crc32(current_header_.crc32, (const Bytef*)data, data_size);

            // 圧縮しながら書き込みを行う
            char* p = &buffer_[0];
            int buffer_size = buffer_.size();
            while (buffer_size > Compress_buffer_size) {
                int n = write_data(p, Compress_buffer_size);
                if (n < 0) {
                    return -3;
                }

                buffer_size -= n;
                p += n;
            }
            buffer_.erase(buffer_.begin(), buffer_.begin() + (p - &buffer_[0]));

            return data_size;
        }

        int read(char* data, int max_data_size)
        {
            if (open_mode_ != ios_base::in) {
                return -1;
            }

            // バッファにデータがあれば、それを格納する
            int filled = 0;
            if (!buffer_.empty()) {
                filled += copy_from_buffer(data, max_data_size);
            }

            // ファイルが open できていなければ、戻る
            if (!is_open()) {
                return (filled > 0) ? filled : -2;
            }

            // 必要量のデータが取得できるまで、処理を行う
            if (fio_->eof()) {
                close_header();
                return -3;
            }

            // 圧縮データの読み込みと zlib への登録
            char buffer[Compress_buffer_size];
            while (filled < max_data_size) {
                int read_size = min(static_cast<int>(Compress_buffer_size),
                                    left_data_size_);
                fio_->read(buffer, read_size);
                int buffer_size = static_cast<int>(fio_->gcount());
                left_data_size_ -= buffer_size;
                zs_.next_in = (Bytef*)buffer;
                zs_.avail_in = buffer_size;

                // 解凍できるサイズの zlib への登録
                int left_buffer_size = max_data_size - filled;
                zs_.next_out = (Bytef*)&data[filled];
                zs_.avail_out = left_buffer_size;

                // 解凍
                int status = inflate(&zs_, Z_NO_FLUSH);
                int store_size = left_buffer_size - zs_.avail_out;
                filled += store_size;

                if ((status == Z_STREAM_END) || (status != Z_OK)) {
                    close_header();
                    break;
                }

                if (filled >= max_data_size) {
                    // 残っているデータを次に読み込めるようにバッファに書き込む
                    store_to_buffer();
                }
            }

            return filled;
        }

        bool eof() const
        {
            if (open_mode_ == ios_base::out) {
                return true;
            }

            if (!buffer_.empty()) {
                return false;
            }

            return (!is_open() || (left_data_size_ <= 0));
        }


        Zip::Headers headers() const
        {
            return headers_;
        }


        bool add_write_header(const string& name, time_t time)
        {
            close_header();

            zip_header_t& header = current_header_;
            memset(&header, 0, sizeof(header));

            header.signature = 0x04034B50;
            header.needver = 20;
            header.option = 0;
            header.comptype = 8;

            header.filetime = dos_time(time);
            header.filedate = dos_date(time);

            // 圧縮後に設定し直す
            header.crc32 = 0;
            header.compsize = 0;
            header.uncompsize = 0;

            header.fnamelen = name.size();
            header.extralen = 0;

            // 書き込み
            long header_position = static_cast<long>(fio_->tellg());

            WRITE_BYTES(fio_, header.signature, 4);
            WRITE_BYTES(fio_, header.needver, 2);
            WRITE_BYTES(fio_, header.option, 2);
            WRITE_BYTES(fio_, header.comptype, 2);
            WRITE_BYTES(fio_, header.filetime, 2);
            WRITE_BYTES(fio_, header.filedate, 2);
            WRITE_BYTES(fio_, header.crc32, 4);
            WRITE_BYTES(fio_, header.compsize, 4);
            WRITE_BYTES(fio_, header.uncompsize, 4);
            WRITE_BYTES(fio_, header.fnamelen, 2);
            WRITE_BYTES(fio_, header.extralen, 2);

            // 名前の書き込み
            fio_->write(name.c_str(), name.size());

            // central_header の準備
            central_t central;
            central_header_t& central_header = central.header;
            memset(&central_header, 0, sizeof(central_header));

            const uint32_t central_header_signature = 0x02014B50;
            central_header.signature = central_header_signature;
            central_header.madever = 20;
            central_header.needver = header.needver;
            central_header.option = header.option;
            central_header.comptype = header.comptype;
            central_header.filetime = header.filetime;
            central_header.filedate = header.filedate;
            central_header.crc32 = 0;
            central_header.compsize = 0;
            central_header.uncompsize = 0;
            central_header.fnamelen = header.fnamelen;
            central_header.extralen = header.extralen;
            central_header.commentlen = 0;
            central_header.disknum = 0;
            central_header.inattr = 0x0;
            central_header.outattr = 0x0;
            central_header.headerpos = header_position;

            central.name = name;
            central_headers_.push_back(central);

            return true;
        }

        void update_zip_header()
        {
            enum { Crc_offset = 4 + 2*5 };
            central_t& last = central_headers_.back();
            central_header_t& last_header = last.header;
            long header_position = last_header.headerpos;
            fio_->seekg(header_position + Crc_offset, fstream::beg);

            WRITE_BYTES(fio_, current_header_.crc32, 4);
            WRITE_BYTES(fio_, current_header_.compsize, 4);
            WRITE_BYTES(fio_, current_header_.uncompsize, 4);
            fio_->seekg(0, fstream::end);

            last_header.crc32 = current_header_.crc32;
            last_header.compsize = current_header_.compsize;
            last_header.uncompsize = current_header_.uncompsize;
        }

        bool open_read_header(const string& name)
        {
            Zipped_data::iterator it = zipped_data_.find(name);
            if (it == zipped_data_.end()) {
                return false;
            }
            const zipped_data_t& data = it->second;

            fio_->seekg(data.headerpos, fstream::beg);
            read_zip_header(current_header_, fio_);
            if ((current_header_.signature & 0xffff) != 0x4b50) {
                return false;
            }

            // 読み出せるサイズの更新
            left_data_size_ = current_header_.compsize;

            // fnamelen, extralen だけ読み飛ばす
            fio_->seekg(current_header_.fnamelen +
                        current_header_.extralen, fstream::cur);

            return true;
        }

        void close_header()
        {
            if (is_open_) {
                if (open_mode_ == ios_base::out) {
                    if ((current_header_.signature != 0x0) &&
                        (current_header_.uncompsize > 0)) {

                        write_data(&buffer_[0], buffer_.size(), Finish);
                        buffer_.clear();
                        deflateReset(&zs_);
                        update_zip_header();

                        memset(&current_header_, 0, sizeof(current_header_));
                    }
                } else if (open_mode_ == ios_base::in) {
                    inflateReset(&zs_);
                }
            }
        }

    private:
        bool is_open_;
        z_stream zs_;
        fstream* fio_;
        ios_base::openmode open_mode_;
        vector<char> buffer_;
        Zip::Headers headers_;
        string handling_name_;
        Zipped_data zipped_data_;
        zip_header_t current_header_;
        int left_data_size_;
        Central_headers central_headers_;

        int write_data(char* data, int data_size, bool is_finish = false)
        {
            int flush_type;
            if (is_finish) {
                flush_type = Z_FINISH;
            } else if (data_size >= Compress_buffer_size) {
                flush_type = Z_SYNC_FLUSH;
            } else {
                flush_type = Z_NO_FLUSH;
            }

            zs_.next_in = (Bytef*)data;
            zs_.avail_in = data_size;

            char buffer[Compress_buffer_size];
            zs_.next_out = (Bytef*)buffer;
            zs_.avail_out = Compress_buffer_size;

            deflate(&zs_, flush_type);

            // avail_out に未圧縮データのサイズが格納されるので
            // 差し引いた分のデータを圧縮データとして出力する
            int store_size = Compress_buffer_size - zs_.avail_out;
            current_header_.compsize += store_size;
            fio_->write(buffer, store_size);

            return data_size;
        }

        void store_to_buffer()
        {
            char buffer[Compress_buffer_size];

            do {
                zs_.next_out = (Bytef*)buffer;
                zs_.avail_out = Compress_buffer_size;

                int status = inflate(&zs_, Z_NO_FLUSH);
                int store_size = Compress_buffer_size - zs_.avail_out;
                buffer_.insert(buffer_.end(), buffer, buffer + store_size);
                if (status == Z_STREAM_END) {
                    close_header();
                    break;
                } else if (status != Z_OK) {
                    break;
                }
            } while (zs_.avail_out == 0);
        }

        int copy_from_buffer(char* data, int max_data_size)
        {
            int copy_size = min(max_data_size,
                                static_cast<int>(buffer_.size()));
            copy(buffer_.begin(), buffer_.begin() + copy_size, data);
            buffer_.erase(buffer_.begin(), buffer_.begin() + copy_size);
            return copy_size;
        }
    };
}


struct Zip::pImpl
{
    Zip_access* raw_;

    pImpl() : raw_(NULL)
    {
    }
};

Zip::Zip() : pimpl(new pImpl)
{
}

Zip::~Zip()
{
    close();
}

bool Zip::is_open() const
{
    if (!pimpl->raw_) {
        return false;
    }
    return pimpl->raw_->is_open();
}

void Zip::close()
{
    delete pimpl->raw_;
    pimpl->raw_ = NULL;
}

int Zip::write(const char* data, size_t data_size)
{
    if (!pimpl->raw_) {
        return -1;
    }
    return pimpl->raw_->write(data, data_size);
}

int Zip::read(char* data, size_t max_data_size, int timeout)
{
    static_cast<void>(timeout);

    if (!pimpl->raw_) {
        return -1;
    }
    return pimpl->raw_->read(data, max_data_size);
}

bool Zip::eof() const
{
    if (!pimpl->raw_) {
        return true;
    }
    return pimpl->raw_->eof();
}

bool Zip::uncompress(const std::string& file_name)
{
    close();

    pimpl->raw_ = new Zip_plain(file_name, "r");
    return pimpl->raw_->is_open();
}

bool Zip::add_write_header(const std::string& name, time_t time)
{
    return pimpl->raw_->add_write_header(name, time);
}

bool Zip::compress(const std::string& file_name)
{
    close();

    pimpl->raw_ = new Zip_plain(file_name, "w");
    return pimpl->raw_->is_open();
}

Zip::Headers Zip::headers() const
{
    return pimpl->raw_->headers();
}

bool Zip::open_read_header(const string& name)
{
    return pimpl->raw_->open_read_header(name);
}

bool Zip::load_data(std::vector<uint8_t>& data, const std::string& data_name)
{
    if (!open_read_header(data_name)) {
        return false;
    }

    enum { Buffer_size = 4096 };
    char buffer[Buffer_size];
    while (!eof()) {
        int n = read(buffer, Buffer_size);
        if (n <= 0) {
            break;
        }
        data.insert(data.end(), buffer, buffer + n);
    }

    return true;
}

bool Zip::load_data(std::string& data, const std::string& data_name)
{
    vector<uint8_t> vector_data;
    if (!load_data(vector_data, data_name)) {
        return false;
    }

    data = string(reinterpret_cast<const char*>(&vector_data[0]),
                  vector_data.size());
    return true;
}

bool Zip::add_data(const std::vector<uint8_t>& data,
                   const std::string& data_name)
{
    const char* p = reinterpret_cast<const char*>(&data[0]);
    const string string_data(p, data.size());
    return add_data(string_data, data_name);
}

bool Zip::add_data(const std::string& data, const std::string& data_name)
{
    if (!add_write_header(data_name, time(NULL))) {
        return false;
    }

    const char* p = reinterpret_cast<const char*>(&data[0]);
    if (write(p, data.size()) != static_cast<int>(data.size())) {
        return false;
    }

    return true;
}

bool Zip::add_file(const std::string& file_name, const std::string& data_name)
{
    struct stat st;
    if (stat(file_name.c_str(), &st) < 0) {
        printf("%s: '%s'\n", strerror(errno), file_name.c_str());
        return false;
    }
    if (!add_write_header(data_name, st.st_mtime)) {
        return false;
    }

    ifstream fin(file_name.c_str(), ios_base::binary);
    enum { Buffer_size = 4096 };
    char buffer[Buffer_size];
    while (!fin.eof()) {
        fin.read(buffer, Buffer_size);
        int n = fin.gcount();
        if (n <= 0) {
            break;
        }

        if (write(buffer, n) != n) {
            return false;
        }
    }

    return true;
}
