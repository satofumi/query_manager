#ifndef THEME_SELECTOR_H
#define THEME_SELECTOR_H

/*!
  \file
  \brief テーマ選択
*/

#include <memory>
#include <QDialog>
#include "ui_Theme_selector_form.h"


class Theme_selector : public QDialog, private Ui::Theme_selector_form
{
    Q_OBJECT;

 public:
    Theme_selector(QWidget* parent = NULL);
    ~Theme_selector();

    void load_themes(const QString& directory_path);
    void set_current_theme_file(const QString& theme_file);

 signals:
    void theme_changed(const QString& theme_file);

 private slots:
    void cancel_button_clicked();
    void ok_button_clicked();
    void item_selection_changed();

 private:
    Theme_selector(const Theme_selector& rhs);
    Theme_selector& operator = (const Theme_selector& rhs);

    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};

#endif
