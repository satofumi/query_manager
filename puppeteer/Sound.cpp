/*!
  \file
  \brief 音データの再生
*/

#include <QMediaPlayer>
#include <QBuffer>
#include "Sound.h"

using namespace std;


namespace
{
    typedef map<string, QMediaPlayer*> sounds_t;
}

struct Sound::pImpl
{
    sounds_t sounds_;


    void stopAll()
    {
        for (sounds_t::iterator it = sounds_.begin();
             it != sounds_.end(); ++it) {
            it->second->stop();
        }
    }
};


Sound::Sound() : pimpl(new pImpl)
{
}


Sound::~Sound()
{
}


void Sound::add(const std::string& key, const std::vector<uint8_t>& data)
{
    QMediaPlayer* player = new QMediaPlayer();

    QBuffer* buffer = new QBuffer(player);
    buffer->setData(reinterpret_cast<const char*>(&data[0]), data.size());
    buffer->open(QIODevice::ReadOnly);

    player->setMedia(QMediaContent(), buffer);
    pimpl->sounds_[key] = player;
}


void Sound::play(const std::string& key)
{
    sounds_t::iterator it = pimpl->sounds_.find(key);
    if (it == pimpl->sounds_.end()) {
        return;
    }

    pimpl->stopAll();
    QMediaPlayer* sound = it->second;
    sound->setPosition(0);
    sound->play();
}
