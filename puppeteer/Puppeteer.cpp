/*!
  \file
  \brief マスコットの操作
*/

#include <QApplication>
#include <QTcpServer>
#include <QTcpSocket>
#include <QPainter>
#include <QAction>
#include <QSettings>
#include <QMouseEvent>
#include <QNetworkInterface>
#include <QMessageBox>
#include <QFileInfo>
#include <QUrl>
#include <QDesktopServices>
#include "Puppeteer.h"
#include "Theme_selector.h"
#include "Balloon.h"
#include "Resource.h"
#include "mascot_setting.h"

#if defined(Q_OS_WIN)
#include <windows.h>
#endif

using namespace std;
using namespace mascot_setting;


namespace
{
    const QString Settings_file = "settings.ini";
    const QString Theme_directory = "mascots/";
    const QString Default_theme = Theme_directory + "sd_query.dat";


    void quit_application()
    {
        QApplication::quit();
        exit(1);
    }
}


struct Puppeteer::pImpl
{
    enum { Port = 17821 };

    Puppeteer* widget_;
    Theme_selector theme_selector_;
    QString ini_path_;
    Balloon balloon_;
    QPoint balloon_offset_;
    QTcpServer server_;
    Resource resource_;
    QPoint drag_position_;
    QString theme_file_;


    pImpl(Puppeteer* widget)
        : widget_(widget), balloon_(widget), server_(widget)
    {
    }

    void initialize_form()
    {
        connect(&resource_, SIGNAL(update_cell()),
                widget_, SLOT(repaint()));
        connect(&resource_, SIGNAL(animation_ended()),
                widget_, SLOT(animation_ended()));
        connect(&resource_, SIGNAL(show_message(const std::string&)),
                widget_, SLOT(show_message(const std::string)));

        connect(&theme_selector_, SIGNAL(theme_changed(const QString&)),
                widget_, SLOT(theme_changed(const QString&)));

        initialize_menu();

        const QString application_dir = qApp->applicationDirPath();
        ini_path_ = application_dir + '/' + Settings_file;

        const QString mascots_path = application_dir + "/mascots";
        theme_selector_.load_themes(mascots_path);
    }

    void initialize_menu()
    {
        // 右クリックで表示するメニュー
        QAction* open_browser_action =
            new QAction(QAction::tr("&open browser"), widget_);
        connect(open_browser_action, SIGNAL(triggered()),
                widget_, SLOT(open_browser()));
        widget_->addAction(open_browser_action);

        QAction* theme_action = new QAction(tr("Change &Theme"), widget_);
        connect(theme_action, SIGNAL(triggered()),
                &theme_selector_, SLOT(show()));
        widget_->addAction(theme_action);

        QAction* quit_action = new QAction(QAction::tr("&Quit"), widget_);
        connect(quit_action, SIGNAL(triggered()), widget_, SLOT(close()));
        widget_->addAction(quit_action);

        widget_->setContextMenuPolicy(Qt::ActionsContextMenu);
    }

    void load_theme()
    {
        const QFileInfo file_info(theme_file_);
        if (!file_info.exists()) {
            theme_file_ = Default_theme;
        }

        try {
            resource_.load(theme_file_);
            widget_->resize(resource_.size());
            save_setting();

        } catch (const QString& message) {
            QMessageBox::critical(widget_, tr("Load theme error"), message);
            quit_application();
        }

        balloon_offset_ = resource_.balloon_offset();
    }

    void initialize_server()
    {
        if (!server_.listen(QHostAddress::Any, Port)) {
            QString message =
                tr("QTcpServer::listen(): ") + server_.errorString();
            QMessageBox::critical(widget_, tr("Server error"), message);
            quit_application();
        }
        QString ip_address;
        QList<QHostAddress> ip_addresses = QNetworkInterface::allAddresses();
        for (int i = 0; i < ip_addresses.size(); ++i) {
            if (ip_addresses.at(i) != QHostAddress::LocalHost &&
                ip_addresses.at(i).toIPv4Address()) {
                ip_address = ip_addresses.at(i).toString();
                break;
            }
        }

        if (ip_address.isEmpty()) {
            ip_address = QHostAddress(QHostAddress::LocalHost).toString();
        }

        connect(&server_, SIGNAL(newConnection()), widget_,
                SLOT(raise_event_action()));
    }

    void stay_on_top()
    {
        Qt::WindowFlags flags =
            Qt::FramelessWindowHint | Qt::X11BypassWindowManagerHint |
            Qt::WindowStaysOnTopHint;
        widget_->setWindowFlags(flags);
#if defined(Q_OS_WIN)
        HWND wid = reinterpret_cast<HWND>(widget_->winId());
        LONG style = GetWindowLong(wid, GWL_EXSTYLE);
        SetWindowLong(wid, GWL_EXSTYLE, style | WS_EX_TOOLWINDOW);
#endif
    }

    void load_setting()
    {
        QSettings settings(ini_path_, QSettings::IniFormat);
        widget_->restoreGeometry(settings.value("geometry").toByteArray());
        theme_file_ = settings.value("theme_file", Default_theme).toString();
    }

    void save_setting()
    {
        QSettings settings(ini_path_, QSettings::IniFormat);
        settings.setValue("geometry", widget_->saveGeometry());
        settings.setValue("theme_file", theme_file_);
    }

    void start_event(const QString& event_name)
    {
        resource_.start_event(event_name);
    }

    void stop_event()
    {
        resource_.stop_event();
    }

    void update_balloon_position(const QPoint& point)
    {
        balloon_.set_base_position(point + balloon_offset_);
    }
};


Puppeteer::Puppeteer(QWidget* parent) : QWidget(parent), pimpl(new pImpl(this))
{
    setAttribute(Qt::WA_TranslucentBackground, true);

    pimpl->initialize_form();
    pimpl->initialize_server();
    pimpl->load_setting();
    pimpl->load_theme();
    pimpl->theme_selector_.set_current_theme_file(pimpl->theme_file_);

    show();
}

Puppeteer::~Puppeteer()
{
}

void Puppeteer::show()
{
    pimpl->start_event("Show");
    pimpl->stay_on_top();
    QWidget::show();
    pimpl->update_balloon_position(pos());
}

void Puppeteer::hide()
{
    pimpl->balloon_.hide();
    QWidget::hide();
    pimpl->stop_event();
}

bool Puppeteer::send_show_command(int timeout_msec)
{
    QTcpSocket socket;
    socket.connectToHost(QHostAddress::LocalHost, pImpl::Port);
    if (!socket.waitForConnected(timeout_msec)) {
        return false;
    }

    socket.write("Show");
    socket.flush();
    socket.close();

    return true;
}

void Puppeteer::closeEvent(QCloseEvent* event)
{
    pimpl->theme_selector_.hide();
    event->accept();
}

void Puppeteer::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    pimpl->resource_.draw(painter, this);
}

void Puppeteer::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton) {
        pimpl->drag_position_ = event->globalPos() - frameGeometry().topLeft();
        event->accept();
        pimpl->start_event("Clicked");
    }
}

void Puppeteer::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() & Qt::LeftButton) {
        const QPoint next_position = event->globalPos() - pimpl->drag_position_;
        move(next_position);
        event->accept();
        pimpl->save_setting();

        pimpl->update_balloon_position(next_position);
    }
}

void Puppeteer::raise_event_action()
{
    QTcpSocket *connection = pimpl->server_.nextPendingConnection();
    connect(connection, SIGNAL(disconnected()),
            connection, SLOT(deleteLater()));

    enum { Tiny_timeout = 100 };
    connection->waitForReadyRead(Tiny_timeout);
    QString command = QString(connection->readAll());
    connection->disconnectFromHost();

    if (command == "Show") {
        show();
    } else if (command == "Hide") {
        hide();
    } else {
        // マスコット非表示中はイベントを無視する
        if (!isVisible()) {
            return;
        }
        pimpl->start_event(command);
    }
}

void Puppeteer::animation_ended()
{
    if (pimpl->balloon_.isVisible()) {
        pimpl->balloon_.hide();
    }
    pimpl->start_event("Idle");
}


void Puppeteer::show_message(const std::string& message)
{
    if ((message.size() == 1) && message[0] == '\0') {
        pimpl->balloon_.hide();
        return;
    }

    if (!message.empty()) {
        pimpl->update_balloon_position(pos());
        pimpl->balloon_.set_message(message);
        pimpl->balloon_.show();
    }
}

void Puppeteer::theme_changed(const QString& theme_file)
{
    pimpl->theme_file_ = theme_file;
    pimpl->load_theme();
    repaint();
}

void Puppeteer::open_browser()
{
    QUrl url = QUrl("http://localhost:17820/");
    QDesktopServices::openUrl(url);
}
