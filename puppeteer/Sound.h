#ifndef SOUND_H
#define SOUND_H

/*!
  \file
  \brief 音データの再生
*/

#include <memory>
#include <string>
#include <vector>


class Sound
{
 public:
    Sound();
    ~Sound();

    void add(const std::string& key, const std::vector<uint8_t>& data);
    void play(const std::string& key);

 private:
    Sound(const Sound& rhs);
    Sound& operator = (const Sound& rhs);

    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};

#endif
