<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>Puppeteer</name>
    <message>
        <location filename="Puppeteer.cpp" line="97"/>
        <source>Change &amp;Theme</source>
        <translation>テーマの変更 (&amp;T)</translation>
    </message>
    <message>
        <location filename="Puppeteer.cpp" line="122"/>
        <source>Load theme error</source>
        <oldsource>Load mascot error</oldsource>
        <translation>テーマの読み込みエラー</translation>
    </message>
    <message>
        <location filename="Puppeteer.cpp" line="133"/>
        <source>QTcpServer::listen(): </source>
        <translation></translation>
    </message>
    <message>
        <location filename="Puppeteer.cpp" line="134"/>
        <source>Server error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QAction</name>
    <message>
        <source>&amp;Hide</source>
        <translation type="vanished">隠す (&amp;H)</translation>
    </message>
    <message>
        <location filename="Puppeteer.cpp" line="92"/>
        <source>&amp;open browser</source>
        <translation>ブラウザの起動 (&amp;o)</translation>
    </message>
    <message>
        <location filename="Puppeteer.cpp" line="102"/>
        <source>&amp;Quit</source>
        <translation>終了 (&amp;Q)</translation>
    </message>
</context>
<context>
    <name>Resource</name>
    <message>
        <location filename="Resource.cpp" line="192"/>
        <source>uncompress fail: </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Theme_selector</name>
    <message>
        <location filename="Theme_selector.cpp" line="64"/>
        <source>Zip::uncompress() fail: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="Theme_selector.cpp" line="69"/>
        <source>Zip::load_data() fail.: </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Theme_selector_form</name>
    <message>
        <location filename="Theme_selector_form.ui" line="14"/>
        <source>Theme List</source>
        <translation>テーマリスト</translation>
    </message>
    <message>
        <location filename="Theme_selector_form.ui" line="92"/>
        <source>&amp;Cancel</source>
        <translation>キャンセル (&amp;C)</translation>
    </message>
    <message>
        <location filename="Theme_selector_form.ui" line="85"/>
        <source>&amp;OK</source>
        <translation></translation>
    </message>
</context>
</TS>
