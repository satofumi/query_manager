#ifndef UTF8_H
#define UTF8_H

/*!
  \file
  \brief UTF-8 文字列の操作クラス
*/

#include <memory>
#include <stdint.h>
#include <string>


//! UTF-8 文字列の操作クラス
class Utf8
{
 public:
    Utf8(void);

    //! 初期値を指定したコンストラクタ
    Utf8(const std::string& utf8_text);

    //! コピーコンストラクタ
    Utf8(const Utf8& rhs);

    //! 代入コンストラクタ
    Utf8& operator = (const Utf8& rhs);

    ~Utf8(void);

    /*!
      \brief データが格納されているかを返す

      \retval true データが格納されている
      \retval false データが格納されていない
    */
    bool empty(void);

    //! 格納されているデータをクリアする
    void clear(void);

    //! UTF-8 の文字数を返す
    size_t size(void) const;

    //! 指定された位置の UTF-8 の文字を返す
    uint32_t ch(size_t index) const;

    //! 指定された位置の UTF-8 の文字を返す
    uint16_t operator[](size_t index) const;

    //! std:string としたデータを返す
    std::string to_string(void) const;

    //! UTF-8 の部分文字列を返す
    Utf8 substr(size_t first_index, size_t n = std::string::npos) const;

    //! 最後の UTF-8 の文字を取り除く
    void pop_back(void);

    Utf8& operator += (const Utf8& rhs);

 private:
    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};


const Utf8 operator + (const Utf8& lhs, const Utf8& rhs);

#endif
