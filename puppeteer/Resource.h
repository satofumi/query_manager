#ifndef RESOURCE_H
#define RESOURCE_H

/*!
  \file
  \brief リソース管理
*/

#include <memory>
#include <QWidget>

class QPainter;


class Resource : public QWidget
{
    Q_OBJECT;

 public:
    Resource(QWidget* parent = NULL);
    ~Resource();

    void load(const QString& resource_file);
    QSize size() const;
    QPoint balloon_offset() const;
    void draw(QPainter& painter, QWidget* widget);

    void start_event(const QString& event_name);
    void stop_event();

 signals:
    void update_cell();
    void animation_ended();
    void show_message(const std::string& message);

 private slots:
    void update_animation_cell();

 private:
    Resource(const Resource& rhs);
    Resource& operator = (const Resource& rhs);

    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};

#endif
