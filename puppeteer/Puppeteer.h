#ifndef PUPPETEER_H
#define PUPPETEER_H

/*!
  \file
  \brief マスコットの操作
*/

#include <memory>
#include <QWidget>


class Puppeteer : public QWidget {
    Q_OBJECT;

 public:
    Puppeteer(QWidget* parent = NULL);
    ~Puppeteer();

    void show();
    void hide();

    static bool send_show_command(int timeout_msec);

 protected:
    void closeEvent(QCloseEvent* event);
    void paintEvent(QPaintEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

 private slots:
    void raise_event_action();
    void animation_ended();
    void show_message(const std::string& message);
    void theme_changed(const QString& theme_file);
    void open_browser();

 private:
    Puppeteer(const Puppeteer& rhs);
    Puppeteer& operator = (const Puppeteer& rhs);

    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};

#endif
