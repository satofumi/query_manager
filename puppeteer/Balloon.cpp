/*!
  \file
  \brief 吹き出し描画
*/

#include <map>
#include <QPainter>
#include <QTextCodec>
#include "Balloon.h"
#include "Utf8.h"

#if defined(Q_OS_WIN)
#include <windows.h>
#endif

using namespace std;

namespace
{
    enum {
        Default_font_size = 18,
        Arc_radius = 16,
        Parts_length = 28,
        From_length = 20,
    };

    const double Line_height = 1.20;

    typedef map<string, QPixmap> Images;
}


struct Balloon::pImpl
{
    Balloon* widget_;
    QString current_message_;
    string font_name_;
    int font_size_;
    Images frame_images_;
    QPoint last_base_position_;
    from_t balloon_from_;
    QRect whole_rect_;
    QRect frame_rect_;
    QRect text_rect_;


    pImpl(Balloon* widget) : widget_(widget),
                             font_size_(Default_font_size),
                             balloon_from_(From_right)
    {
        load_frame_images();
    }

    void load_frame_images()
    {
        const char* tags[] = {
            "center", "top", "right", "bottom", "left",
            "left_top", "right_top", "right_bottom", "left_bottom",
            "from", "from_left", "from_right",
        };

        size_t n = sizeof(tags) / sizeof(tags[0]);
        for (size_t i = 0; i < n; ++i) {
            const string key = tags[i];
            const string file = ":/frame/" + key;
            frame_images_[key] = QPixmap(file.c_str());
        }
    }

    void stay_on_top()
    {
        Qt::WindowFlags flags =
            Qt::FramelessWindowHint | Qt::X11BypassWindowManagerHint |
            Qt::WindowStaysOnTopHint | Qt::Window;
        widget_->setWindowFlags(flags);
#if defined(Q_OS_WIN)
        HWND wid = reinterpret_cast<HWND>(widget_->winId());
        LONG style = GetWindowLong(wid, GWL_EXSTYLE);
        SetWindowLong(wid, GWL_EXSTYLE, style | WS_EX_TOOLWINDOW);
#endif
    }

    void draw_balloon(QPainter& painter,
                      const QRect& frame_rect, const QRect& text_rect)
    {
        painter.setRenderHint(QPainter::SmoothPixmapTransform);

        draw_frame(painter, frame_rect);
        draw_text(painter, frame_rect.topLeft() + text_rect.topLeft());
    }

    void calculate_rects(QRect& whole_rect, QRect& frame_rect, QRect& text_rect)
    {
        const QSize text_size = calculate_text_size();
        const int frame_width = text_size.width() + Arc_radius * 2;
        int frame_height = text_size.height() + Arc_radius * 2;
        int frame_x_offset = 0;
        int text_y = Arc_radius;
        QPoint from;

        // メッセージサイズに合わせてリサイズ
        int whole_height = frame_height;
        int whole_width = frame_width;
        switch (balloon_from_) {
        case From_left:
            whole_width += From_length;
            frame_x_offset = From_length;
            frame_height = max(frame_height, From_length + (Parts_length * 2));
            whole_height = max(whole_height, frame_height);
            text_y = (frame_height - text_size.height()) / 2;
            from = QPoint(4, frame_height / 2);
            break;

        case From_right:
            whole_width += From_length;
            frame_height = max(frame_height, From_length + (Parts_length * 2));
            whole_height = max(whole_height, frame_height);
            text_y = (frame_height - text_size.height()) / 2;
            from = QPoint(frame_width + From_length - 4, frame_height / 2);
            break;

        case From_bottom:
        default:
            whole_height += From_length;
            from = QPoint(bottom_from_x(frame_width) + From_length / 2,
                          frame_height + From_length - 4);
            break;
        }

        whole_rect = QRect(last_base_position_ - from,
                           QSize(whole_width, whole_height));
        frame_rect = QRect(frame_x_offset, 0, frame_width, frame_height);
        text_rect = QRect(QPoint(Arc_radius, text_y), text_size);
    }

    QSize calculate_text_size()
    {
        QStringList lines = message_lines();

        int width = 0;
        int n = lines.size();
        for (int y = 0; y < n; ++y) {
            const QString line = lines[y];
            const Utf8 message(line.toStdString());

            int char_size = 0;
            for (size_t i = 0; i < message.size(); ++i) {
                uint32_t ch = message.ch(i);
                char_size += isascii(ch) ? 1 : 2;
            }

            int line_width = font_size_ * char_size / 2;
            width = max(width, line_width);
        }
        return QSize(width, font_size_ * Line_height * n);
    }

    void draw_frame(QPainter& painter, const QRect& frame_rect)
    {
        // 外枠の描画
        const int width = frame_rect.width();
        const int height = frame_rect.height();

        const int right = width - Parts_length;
        const int bottom = height - Parts_length;
        const int center_width = width - (Parts_length * 2);
        const int middle_height = height - (Parts_length * 2);

        const int x_offset = frame_rect.x();
        const QSize Parts_size(Parts_length, Parts_length);
        draw_frame_parts(painter, "left_top",
                         QRect(QPoint(x_offset, 0), Parts_size));
        draw_frame_parts(painter, "right_top",
                         QRect(QPoint(x_offset + right, 0), Parts_size));
        draw_frame_parts(painter, "left_bottom",
                         QRect(QPoint(x_offset, bottom), Parts_size));
        draw_frame_parts(painter, "right_bottom",
                         QRect(QPoint(x_offset + right, bottom), Parts_size));

        draw_frame_parts(painter, "top",
                         QRect(QPoint(x_offset + Parts_length, 0),
                               QSize(center_width, Parts_length)));
        draw_frame_parts(painter, "bottom",
                         QRect(QPoint(x_offset + Parts_length, bottom),
                               QSize(center_width, Parts_length)));
        draw_frame_parts(painter, "right",
                         QRect(QPoint(x_offset + right, Parts_length),
                               QSize(Parts_length, middle_height)));
        draw_frame_parts(painter, "left",
                         QRect(QPoint(x_offset, Parts_length),
                               QSize(Parts_length, middle_height)));

        draw_frame_parts(painter, "center",
                         QRect(QPoint(x_offset + Parts_length, Parts_length),
                               QSize(center_width, middle_height)));

        // 吹き出し元の描画
        switch (balloon_from_) {
        case From_left:
            draw_frame_parts(painter, "from_left",
                             QRect(QPoint(4, Parts_length),
                                   QSize(From_length, From_length)));
            break;

        case From_right:
            draw_frame_parts(painter, "from_right",
                             QRect(QPoint(width - 4, Parts_length),
                                   QSize(From_length, From_length)));
            break;

        case From_bottom:
        default:
            draw_frame_parts(painter, "from",
                             QRect(QPoint(bottom_from_x(frame_rect.width()),
                                          height - 4),
                                   QSize(From_length, From_length)));
            break;
        }
    }

    void draw_frame_parts(QPainter& painter,
                          const string& name, const QRect& dest)
    {
        const QPixmap& pixmap = frame_images_[name];
        painter.drawPixmap(dest, pixmap, pixmap.rect());
    }

    void draw_text(QPainter& painter, const QPoint& message_left_top)
    {
        QFont font;
        font.setFamily(font_name_.c_str());
        font.setPixelSize(font_size_);
        font.setStyleHint(QFont::Courier);
        painter.setFont(font);

        QTextCodec* codec = QTextCodec::codecForName("UTF-8");
        QStringList lines = message_lines();
        for (int i = 0; i < lines.size(); ++i) {
            int x = 0;
            int y = (font_size_ * Line_height * i) + font_size_;

            QString message = !codec ?
                lines[i] : codec->toUnicode(lines[i].toStdString().c_str());
            painter.drawText(message_left_top + QPoint(x, y), message);
        }
    }

    QStringList message_lines()
    {
        return current_message_.split(QChar('\n'));
    }

    int bottom_from_x(int frame_width)
    {
        if (frame_width < (Parts_length * 3)) {
            return Parts_length;
        } else {
            return frame_width * 2 / 3;
        }
    }

    void update_balloon()
    {
        if (current_message_.isEmpty()) {
            return;
        }
        calculate_rects(whole_rect_, frame_rect_, text_rect_);
        widget_->move(whole_rect_.topLeft());
        widget_->resize(whole_rect_.size());

        widget_->repaint();
    }
};


Balloon::Balloon(QWidget* parent) : QWidget(parent), pimpl(new pImpl(this))
{
    setAttribute(Qt::WA_TranslucentBackground, true);
}

Balloon::~Balloon()
{
}

void Balloon::show()
{
    pimpl->stay_on_top();
    QWidget::show();
}

void Balloon::set_message(const std::string& message)
{
    pimpl->current_message_ = message.c_str();
    pimpl->update_balloon();
    raise();
}

void Balloon::set_font_name(const std::string font_name)
{
    pimpl->font_name_ = font_name;
}

void Balloon::set_font_size(int size)
{
    pimpl->font_size_ = size;
}

void Balloon::set_base_position(const QPoint& position, from_t from)
{
    pimpl->last_base_position_ = position;
    pimpl->balloon_from_ = from;
    pimpl->update_balloon();
}

void Balloon::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    pimpl->draw_balloon(painter, pimpl->frame_rect_, pimpl->text_rect_);
}
