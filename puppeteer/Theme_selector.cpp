/*!
  \file
  \brief テーマ選択
*/

#include <QDir>
#include <QListWidget>
#include <QScrollBar>
#include <QShortcut>
#include "Theme_selector.h"
#include "Zip.h"

using namespace std;


namespace
{
    const QSize Theme_logo_size(256, 128);
}

struct Theme_selector::pImpl
{
    Theme_selector* widget_;
    QString selected_theme_file_;


    pImpl(Theme_selector* widget) : widget_(widget)
    {
    }

    void initialize_form()
    {
        connect(widget_->cancel_button_, SIGNAL(clicked()),
                widget_, SLOT(cancel_button_clicked()));
        connect(widget_->ok_button_, SIGNAL(clicked()),
                widget_, SLOT(ok_button_clicked()));

        connect(widget_->list_, SIGNAL(itemSelectionChanged()),
                widget_, SLOT(item_selection_changed()));

        new QShortcut(Qt::CTRL + Qt::Key_W, widget_, SLOT(close()));

        QListWidget* list = widget_->list_;
        list->setSpacing(1);
        list->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    }

    void add_theme(const QString& theme_file)
    {
        QListWidget* list = widget_->list_;

        QPixmap logo_image = load_logo_image(theme_file);
        QListWidgetItem* item =
            new QListWidgetItem(QIcon(logo_image), QString());
        item->setData(Qt::UserRole, theme_file);
        item->setTextAlignment(Qt::AlignHCenter);
        list->addItem(item);
    }

    QPixmap load_logo_image(const QString& theme_file)
    {
        Zip zip;
        if (!zip.uncompress(theme_file.toStdString())) {
            throw tr("Zip::uncompress() fail: ") + theme_file;
        }

        vector<unsigned char> data;
        if (!zip.load_data(data, "logo.png")) {
            throw tr("Zip::load_data() fail.: ") + theme_file;
        }

        QPixmap image;
        image.loadFromData(&data[0], data.size());

        return image;
    }

    void theme_changed(QListWidgetItem* item)
    {
        const QString file_name = item->data(Qt::UserRole).toString();
        widget_->emit theme_changed(file_name);
    }

    void change_theme(const QString& theme_file)
    {
        const size_t n = widget_->list_->count();
        for (size_t i = 0; i < n; ++i) {
            QListWidgetItem* item = widget_->list_->item(i);
            const QString item_theme_file = item->data(Qt::UserRole).toString();
            if (theme_file == item_theme_file) {
                theme_changed(item);
                return;
            }
        }
    }
};


Theme_selector::Theme_selector(QWidget* parent) : QDialog(parent),
                                                  pimpl(new pImpl(this))
{
    setupUi(this);
    pimpl->initialize_form();
}


Theme_selector::~Theme_selector()
{
}


void Theme_selector::load_themes(const QString& directory_path)
{
    list_->setIconSize(Theme_logo_size);
    list_->clear();

    // .dat ファイルからロゴ画像を読み取り、パスと関連付けて管理する
    QDir dir(directory_path);
    dir.setFilter(QDir::Files | QDir::Readable);
    dir.setSorting(QDir::Name);

    QStringList filters;
    filters << "*.dat";
    dir.setNameFilters(filters);

    QFileInfoList list = dir.entryInfoList();
    for (int i = 0; i < list.size(); ++i) {
        pimpl->add_theme(list.at(i).filePath());
    }
}

void Theme_selector::set_current_theme_file(const QString& theme_file)
{
    pimpl->selected_theme_file_ = theme_file;
}

void Theme_selector::cancel_button_clicked()
{
    pimpl->change_theme(pimpl->selected_theme_file_);
    close();
}

void Theme_selector::ok_button_clicked()
{
    // 選択されているテーマにする
    QListWidgetItem* item = list_->currentItem();
    if (item) {
        set_current_theme_file(item->data(Qt::UserRole).toString());
    }
    close();
}

void Theme_selector::item_selection_changed()
{
    QListWidgetItem* item = list_->currentItem();
    if (item) {
        pimpl->theme_changed(item);
    }
}
