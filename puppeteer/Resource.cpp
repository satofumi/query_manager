/*!
  \file
  \brief リソース管理
*/

#include <cstdlib>
#include <QDebug>
#include <QPixmap>
#include <QBitmap>
#include <QPainter>
#include <QTimer>
#include "Resource.h"
#include "Sound.h"
#include "mascot_setting.h"
#include "Zip.h"

using namespace std;
using namespace mascot_setting;


namespace
{
    typedef std::map<std::string, QPixmap> cell_images_t;
    typedef std::map<std::string, QPixmap> draw_images_t;
}


struct Resource::pImpl
{
    Resource* widget_;
    QTimer update_timer_;
    animation_images_t animation_images_;
    string draw_image_name_;
    size_t animation_index_;
    draw_images_t draw_images;
    mascot_t mascot_;
    std::vector<std::string> messages_;
    Sound sound_;


    pImpl(Resource* widget) : widget_(widget)
    {
        srand(time(NULL));

        update_timer_.setSingleShot(true);
        connect(&update_timer_, SIGNAL(timeout()),
                widget_, SLOT(update_animation_cell()));
    }

    void unload()
    {
        draw_images.clear();
        messages_.clear();

        mascot_.size = QSize(0, 0);
        mascot_.cells.clear();
        mascot_.images.clear();
        mascot_.animations.clear();
        mascot_.sounds.clear();
        mascot_.events.clear();
        mascot_.balloon_offset = QPoint(0, 0);
    }

    QPixmap load_image(const string& image_file, Zip& zip)
    {
        if (image_file.empty()) {
            throw QString(("Image file name is empty: " + image_file).c_str());
        }

        vector<uint8_t> data;
        if (!zip.load_data(data, image_file)) {
            throw QString(("No image file: " + image_file).c_str());
        }

        QPixmap image;
        image.loadFromData(&data[0], data.size());

        return image;
    }

    void start_animation(const string& animation_name)
    {
        animations_t::const_iterator it =
            mascot_.animations.find(animation_name);
        if (it == mascot_.animations.end()) {
            return;
        }

        animation_images_ = it->second;
        animation_index_ = 0;
        update_animation_cell();
    }

    void update_animation_cell()
    {
        if (animation_index_ >= animation_images_.size()) {
            emit widget_->animation_ended();
            return;
        }
        const animation_image_t& animation_image =
            animation_images_[animation_index_];

        string message = animation_image.message;
        if (!message.empty()) {
            if ((message == "%s") && !messages_.empty()) {
                message = messages_.front();
                messages_.erase(messages_.begin(), messages_.begin());
            }
        }
        emit widget_->show_message(message);

        draw_image_name_ = animation_image.image_name;

        ++animation_index_;
        emit widget_->update_cell();

        update_timer_.start(animation_image.delay_msec);
    }

    void draw_image(QPainter& painter, QWidget* widget)
    {
        const QPixmap& image = draw_images[draw_image_name_];
        widget->clearMask();
        painter.drawPixmap(QPoint(0, 0), image);
        widget->setMask(image.createMaskFromColor(Qt::transparent));
    }

    const event_t& select_event(const event_set_t& event_set)
    {
        const double r = double(rand()) / RAND_MAX;
        double total_threshold = 0.0;
        for (event_set_t::const_iterator it = event_set.begin();
             it != event_set.end(); ++it) {
            const event_t& event = *it;
            total_threshold += event.probability;
            if (r <= total_threshold) {
                return *it;
            }
        }

        return event_set.front();
    }

    void create_images(const mascot_t& mascot, const cell_images_t& cell_images)
    {
        for (images_t::const_iterator it = mascot.images.begin();
             it != mascot.images.end(); ++it) {
            const string& image_name = it->first;
            const image_cells_t& image_cells = it->second;

            QPixmap image(mascot.size);
            image.fill(Qt::transparent);

            QPainter dest(&image);
            dest.setCompositionMode(QPainter::CompositionMode_SourceOver);

            for (image_cells_t::const_iterator cell_it = image_cells.begin();
                 cell_it != image_cells.end(); ++cell_it) {
                const string& cell_name = cell_it->cell_name;

                cell_images_t::const_iterator cell_image_it =
                    cell_images.find(cell_name);
                if (cell_image_it == cell_images.end()) {
                    return;
                }

                const QPixmap& cell_image = cell_image_it->second;
                const QPoint position = cell_it->position;
                const QRect offset = cell_it->offset;

                QRect actual_offset;
                if ((offset.width() == 0) || (offset.height() == 0)) {
                    actual_offset = QRect(QPoint(0, 0), cell_image.size());
                } else {
                    actual_offset = offset;
                }

                // 重ねあわせるセル画像を作成する
                const QPixmap parts_image = cell_image.copy(actual_offset);

                // 画像を重ねあわせる
                dest.drawPixmap(position, parts_image);
            }
            draw_images[image_name] = image;
        }
    }

    void register_sounds(const mascot_setting::sounds_t& sounds, Zip& zip)
    {
        for (mascot_setting::sounds_t::const_iterator it = sounds.begin();
             it != sounds.end(); ++it) {
            const string& key = it->first;
            const string& file_name = it->second;

            vector<uint8_t> data;
            if (!zip.load_data(data, file_name)) {
                throw QString(("No sound file: " + file_name).c_str());
            }
            sound_.add(key, data);
        }
    }
};


Resource::Resource(QWidget* parent) : QWidget(parent), pimpl(new pImpl(this))
{
}

Resource::~Resource()
{
}

void Resource::load(const QString& resource_file)
{
    Zip zip;
    if (!zip.uncompress(resource_file.toStdString())) {
        throw tr("uncompress fail: ") + resource_file;
    }
    pimpl->unload();

    // 設定ファイルの読み出し
    mascot_t& mascot = pimpl->mascot_;
    string setting_yaml;
    zip.load_data(setting_yaml, "setting.yaml");
    mascot_setting::load(mascot, setting_yaml);

    // Cell リソースの読み込み
    cell_images_t cell_images;
    for (mascot_setting::cells_t::const_iterator it = mascot.cells.begin();
         it != mascot.cells.end(); ++it) {
        const string& cell_name = it->first;
        cell_images[cell_name] = pimpl->load_image(it->second, zip);
    }

    // Image リソースの合成
    pimpl->create_images(mascot, cell_images);

    // Sound リソースの読み込み
    pimpl->register_sounds(mascot.sounds, zip);
}

QSize Resource::size() const
{
    return pimpl->mascot_.size;
}

QPoint Resource::balloon_offset() const
{
    return pimpl->mascot_.balloon_offset;
}

void Resource::draw(QPainter& painter, QWidget* widget)
{
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    pimpl->draw_image(painter, widget);
}

void Resource::start_event(const QString& event_name)
{
    events_t::const_iterator it =
        pimpl->mascot_.events.find(event_name.toStdString());
    if (it == pimpl->mascot_.events.end()) {
        qDebug() << "no event: " << event_name << endl;
        return;
    }
    const event_set_t& event_set = it->second;

    // どのアニメーションを再生するかを決定する
    const event_t& event = pimpl->select_event(event_set);
    pimpl->messages_ = event.messages;

    // アニメーションに対応する音データを再生する
    if (!event.sound_name.empty()) {
        pimpl->sound_.play(event.sound_name);
    }

    pimpl->start_animation(event.animation_name);
}

void Resource::stop_event()
{
    pimpl->update_timer_.stop();
}

void Resource::update_animation_cell()
{
    pimpl->update_animation_cell();
}
