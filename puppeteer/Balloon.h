#ifndef BALLOON_H
#define BALLOON_H

/*!
  \file
  \brief 吹き出し描画
*/

#include <memory>
#include <QWidget>


class Balloon : public QWidget
{
    Q_OBJECT;

 public:
    typedef enum
    {
        From_bottom,
        From_right,
        From_left,
    } from_t;

    Balloon(QWidget* parent = NULL);
    ~Balloon();

    void show();

    void set_message(const std::string& message);

    void set_font_name(const std::string font_name);
    void set_font_size(int size);
    void set_base_position(const QPoint& position, from_t from = From_bottom);

 protected:
    void paintEvent(QPaintEvent* event);

 private:
    Balloon(const Balloon& rhs);
    Balloon& operator = (const Balloon& rhs);

    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};

#endif
