#ifndef LOAD_MASCOT_SETTING_H
#define LOAD_MASCOT_SETTING_H

/*!
  \file
  \brief マスコット設定の読み込み
*/

#include <vector>
#include <map>
#include <string>
#include <QString>
#include <QSize>
#include <QPoint>
#include <QRect>


namespace mascot_setting
{
    // cell
    typedef std::map<std::string, std::string> cells_t;

    // image
    typedef struct {
        std::string cell_name;
        QPoint position;
        QRect offset;
    } image_t;

    typedef std::vector<image_t> image_cells_t;
    typedef std::map<std::string, image_cells_t> images_t;

    // animation
    typedef struct {
        std::string image_name;
        int delay_msec;
        std::string message;
    } animation_image_t;

    typedef std::vector<animation_image_t> animation_images_t;
    typedef std::map<std::string, animation_images_t> animations_t;

    // sound
    typedef std::map<std::string, std::string> sounds_t;

    // event
    typedef struct {
        double probability;
        std::string animation_name;
        std::vector<std::string> messages;
        std::string sound_name;
    } event_t;

    typedef std::vector<event_t> event_set_t;
    typedef std::map<std::string, event_set_t> events_t;

    // mascot
    typedef struct {
        QSize size;
        cells_t cells;
        images_t images;
        animations_t animations;
        sounds_t sounds;
        events_t events;

        QPoint balloon_offset;
    } mascot_t;


    extern void load(mascot_t& mascot, const std::string& yaml_text);
}

#endif
