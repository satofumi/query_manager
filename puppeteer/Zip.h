﻿#ifndef ZIP_H
#define ZIP_H

/*!
  \file
  \brief zip ファイルの処理
*/

#include <stdint.h>
#include <memory>
#include <time.h>
#include <string>
#include <vector>


class Zip
{
 public:
    typedef struct
    {
        std::string name;
        time_t date;
    } header_t;
    typedef std::vector<Zip::header_t> Headers;

    Zip();
    ~Zip();

    bool is_open() const;
    void close();
    int write(const char* data, size_t data_size);
    int read(char* data, size_t max_data_size, int timeout = 0);
    bool eof() const;

    bool uncompress(const std::string& file_name);
    Zip::Headers headers() const;
    bool open_read_header(const std::string& name);

    bool compress(const std::string& file_name);
    bool add_write_header(const std::string& name, time_t time);

    bool load_data(std::vector<uint8_t>& data, const std::string& data_name);
    bool load_data(std::string& data, const std::string& data_name);

    bool add_data(const std::vector<uint8_t>& data,
                  const std::string& data_name);
    bool add_data(const std::string& data, const std::string& data_name);
    bool add_file(const std::string& file_name,
                  const std::string& data_name);

 private:
    Zip(const Zip& rhs);
    Zip& operator = (const Zip& rhs);

    struct pImpl;
    std::unique_ptr<pImpl> pimpl;
};

#endif
