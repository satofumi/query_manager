/*!
  \file
  \brief マスコット設定の読み込み
*/

#include <yaml-cpp/yaml.h>
#include "mascot_setting.h"

using namespace std;
using namespace YAML;
using namespace mascot_setting;


namespace
{
    void operator >> (const Node& node, QSize& size)
    {
        const string size_line = node.as<string>("");
        if (size_line.empty()) {
            throw "parse error: QSize";
        }

        const char* line = const_cast<char*>(size_line.c_str());
        char* p;
        int w = strtol(line, &p, 10);
        int h = strtol(++p, NULL, 10);
        size = QSize(w, h);
    }

    void operator >> (const Node& node, QPoint& point)
    {
        const string point_line = node.as<string>("");
        if (point_line.empty()) {
            throw "parse error: QPoint";
        }

        const char* line = const_cast<char*>(point_line.c_str());
        char* p;
        int x = strtol(line, &p, 10);
        int y = strtol(p, NULL, 10);
        point = QPoint(x, y);
    }

    void operator >> (const Node& node, QRect& rect)
    {
        const string rect_line = node.as<string>("");
        if (rect_line.empty()) {
            throw "parse error: QRect";
        }

        const char* line = const_cast<char*>(rect_line.c_str());
        char* p;
        int w = strtol(line, &p, 10);
        int h = strtol(++p, &p, 10);
        int x = strtol(p, &p, 10);
        int y = strtol(p, NULL, 10);
        rect = QRect(x, y, w, h);
    }

    void operator >> (const Node& node, cells_t& cells)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            const string cell_name = it->first.as<string>("");
            const string cell_file = it->second.as<string>("");
            cells[cell_name] = cell_file;
        }
    }

    void parse_position_offset(const Node& node,
                               QPoint& position, QRect& offset)
    {
        node[0] >> position;
        if (node.size() >= 2) {
            node[1] >> offset;
        }
    }

    void parse_image(const Node& node, image_t& image)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            image.cell_name = it->first.as<string>("");
            parse_position_offset(it->second, image.position, image.offset);
        }
    }

    void parse_image_cells(const Node& node, image_cells_t& image_cells)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            image_t image;
            parse_image(*it, image);
            image_cells.push_back(image);
        }
    }

    void operator >> (const Node& node, images_t& images)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            const string image_name = it->first.as<string>("");
            image_cells_t image_cells;
            parse_image_cells(it->second, image_cells);
            images[image_name] = image_cells;
        }
    }

    void parse_msc_message(const Node& node, int& delay_msec, string& message)
    {
        delay_msec = node[0].as<int>(0);
        if (node.size() >= 2) {
            message = node[1].as<string>("");
        }
    }

    void parse_animation_image(const Node& node,
                               animation_image_t& animation_image)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            animation_image.image_name = it->first.as<string>("");
            parse_msc_message(it->second, animation_image.delay_msec,
                              animation_image.message);
        }
    }

    void parse_animation_images(const Node& node,
                                animation_images_t& animation_images)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            animation_image_t animation_image;
            parse_animation_image(*it, animation_image);
            animation_images.push_back(animation_image);
        }
    }

    void operator >> (const Node& node, animations_t& animations)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            const string animation_name = it->first.as<string>("");
            animation_images_t animation_images;
            parse_animation_images(it->second, animation_images);
            animations[animation_name] = animation_images;
        }
    }

    void parse_message(const Node& node, vector<string>& messages)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            messages.push_back(it->as<string>(""));
        }
    }

    void parse_event_set(const Node& node, event_set_t& event_set)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            const Node& items = *it;
            event_t event;
            event.probability = items[0].as<double>(0.0);
            event.animation_name = items[1].as<string>("");
            if (items.size() >= 3) {
                parse_message(items[2], event.messages);
            }
            if (items.size() >= 4) {
                event.sound_name = items[3].as<string>("");
            }
            event_set.push_back(event);
        }
    }

    void operator >> (const Node& node, events_t& events)
    {
        for (Node::const_iterator it = node.begin(); it != node.end(); ++it) {
            const string event_name = it->first.as<string>("");
            event_set_t event_set;
            parse_event_set(it->second, event_set);
            events[event_name] = event_set;
        }
    }

    void operator >> (const Node& node, mascot_t& mascot)
    {
        node["Size"] >> mascot.size;
        node["Cells"] >> mascot.cells;
        node["Images"] >> mascot.images;
        node["Animations"] >> mascot.animations;
        node["Sounds"] >> mascot.sounds;
        node["Events"] >> mascot.events;
        node["Balloon_offset"] >> mascot.balloon_offset;
    }
}


void mascot_setting::load(mascot_t& mascot, const std::string& yaml_text)
{
    YAML::Load(yaml_text) >> mascot;
}
