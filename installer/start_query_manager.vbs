Dim wshShell
Set wshShell = CreateObject("WScript.Shell")

Dim wshEnv
Set wshEnv = wshShell.Environment("Process")

wshEnv.Item("PATH") = wshShell.ExpandEnvironmentStrings("%UserProfile%\AppData\Local\Programs\Python\Python311;%UserProfile%\AppData\Local\Programs\Python\Python311\Scripts") & ";" & wshShell.ExpandEnvironmentStrings("%PATH%")


wshShell.CurrentDirectory = "web"

Dim netstat, re
Set netstat = wshShell.Exec("NETSTAT -an")
Set re = new regexp
re.Pattern = "TCP.*0.0.0.0:17820.*LISTENING"
If Not re.Test(netstat.StdOut.ReadAll()) Then

wshShell.Run "workon query_manager & pserve production.ini", 0, False

End If


wshShell.CurrentDirectory = "..\puppeteer"
wshShell.Run "puppeteer", 1, False

WScript.Sleep 1000

wshShell.Run "http://localhost:17820", 1

' "TCP\s*0.0.0.0:17820\s.*LISTENING"
