Dim wshShell
Set wshShell = CreateObject("WScript.Shell")

Dim wshEnv
Set wshEnv = wshShell.Environment("Process")

wshEnv.Item("PATH") = wshShell.ExpandEnvironmentStrings("%UserProfile%\AppData\Local\Programs\Python\Python311;%UserProfile%\AppData\Local\Programs\Python\Python311\Scripts") & ";" & wshShell.ExpandEnvironmentStrings("%PATH%")

wshShell.Run "%UserProfile%\AppData\Local\Programs\Python\Python311\python -m pip install virtualenvwrapper-win", 1, True

Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")

If fso.FolderExists("%UserProfile%\Envs\query_manager") = False Then
wshShell.Run "mkvirtualenv query_manager", 1, True
End If

wshShell.CurrentDirectory = "web"
wshShell.Run "workon query_manager & python -m pip install --upgrade pip & pip install bcrypt & python setup.py develop & initialize_query_manager_db production.ini & alembic upgrade head", 1, True

MsgBox "The installation was successful.", , "Query Manager Setup"
