include package_version.mk

DIST_DIR = $(PACKAGE_DIR)/puppeteer/mascots

all :
	cd mascots/ && $(MAKE)
	mkdir -p $(DIST_DIR)
	cp mascots/*.dat $(DIST_DIR)/
