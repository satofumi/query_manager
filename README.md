# クエリちゃんのタスク管理

個人向けのシンプルなチケット管理システムです。

 - チケットを終了するとクエリちゃんが褒めてくれて承認欲求が満たされまくる。
 - 個人用に特化されており、サーバを介さずに動作するのでレスポンスが早い。
 - Burn up chart でプロジェクトの進み具合が確認できる。


## ライセンス

 - MIT License


## インストール手順

 - 初期化
   - パッケージを展開する。
   - initialize_query_manager.vbs を実行する。

 - 起動
   - start_query_manager.vbs を実行する。
   - http://localhost:17820 にアクセスする。
     - 「クエリちゃん１７才, ８月２０日が誕生日」と覚えましょう。

## capture_mascot_image の使い方

  - Unity の画面サイズを 256x256 にして実行します。


## 連絡先

バグや要望がありましたら、開発サイトの課題トラッカーにお知らせ下さい。

https://bitbucket.org/satofumi/query_manager/issues


## 素材
  - クエリちゃん
    - http://www.query-chan.com/
