include package_version.mk

DIST_DIR = $(PACKAGE_DIR)/puppeteer

all :
	mkdir -p $(DIST_DIR) $(QT5_PLATFORMS_DIR)
	cp puppeteer/release/puppeteer.exe puppeteer/puppeteer_ja.qm $(DIST_DIR)
	windeployqt $(DIST_DIR)/puppeteer.exe
	cd $(DIST_DIR) && $(RM) -rf bearer iconengines imageformats translations D3Dcompiler_47.dll libEGL.dll libGLESV2.dll opengl32sw.dll Qt5Svg.dll
